<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['slug'];

    public function products() {
        return $this->belongsToMany(Product::class, 'product_promotion');
    }

    public function scopeUnexpired($query) {
        return $query->whereDate('expires_at', '>=', date('Y-m-d'));
    }

    public function getProductIds() {
        return $this->products()
            ->select('products.id')
            ->get()
            ->pluck('id');
    }
}
