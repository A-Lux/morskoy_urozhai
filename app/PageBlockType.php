<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageBlockType extends Model
{
    protected $fillable = ['name', 'option'];
}