<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['slug'];

    public function scopeMain($query) {
        return $query->where('is_main', 1);
    }

    public function bodyShortened() {
        return Str::limit(strip_tags($this->body), 150, '...');
    }
}
