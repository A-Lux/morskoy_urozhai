<?php

namespace App\Providers;

use App\Page\Page;
use App\Page as PageModel;
use App\Facades\Page as PageFacade;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class PageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('page', function () {
            return new Page();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $uri = request()->path();
            $pageInstance = PageModel::where('url', $uri)->with('blocks')->first();
            if ($pageInstance) {
                PageFacade::setTitle($pageInstance->title);
                PageFacade::setDescription($pageInstance->description);
                $blocks = $pageInstance->blocks()
                    ->with('translations')
                    ->select([
                        'page_blocks.id',
                        'page_blocks.slug',
                        'page_block_types.name',
                        'page_blocks.content'
                    ])
                    ->join(
                        'page_block_types',
                        'page_block_types.id',
                        'page_block_type_id'
                    )
                    ->get();
                PageFacade::setBlocks($blocks);
            }
        } catch (\Throwable $th) {

        }
    }
}
