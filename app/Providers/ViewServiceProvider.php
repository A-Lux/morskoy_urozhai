<?php

namespace App\Providers;

use App\Promotion;
use App\Facades\Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\View\Composers\MainBlogComposer;
use App\View\Composers\CartTotalPriceComposer;
use App\View\Composers\PopularProductsComposer;
use App\View\Composers\ProductCategoriesComposer;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $totalPrice = Cart::totalPrice();
        View::composer('layouts.app', function($view) use($totalPrice) {
            $promotions = Promotion::unexpired()->get();
            $view->with(['total' => $totalPrice, 'promotions' => $promotions]);
        });

        View::composer(
            ['partials.popular'],
            PopularProductsComposer::class
        );
        View::composer(
            ['layouts.app', 'home.index'],
            ProductCategoriesComposer::class
        );
        View::composer(
            ['home.index'],
            MainBlogComposer::class
        );
    }
}
