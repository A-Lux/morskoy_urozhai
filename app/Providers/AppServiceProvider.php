<?php

namespace App\Providers;

use App\Cart\Cart;
use Carbon\Carbon;
use App\Facades\Page;
use App\Paybox\Paybox;
use App\Viewer\Viewer;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('cart', function () {
            return new Cart();
        });

        App::bind('viewer', function () {
            return new Viewer();
        });

        App::bind('paybox', function () {
            return new Paybox(env('PAYBOX_MODE'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Blade::directive('pageBlock', function ($expression) {
            eval('$params = ' . $expression . ';');
            
            if(Page::getBlock($params['block'])) {
                $block = $params['block'];
                $property = $params['property'];
                return "<?php if(auth()->check() && auth()->user()->isAdmin()) { echo htmlentities(Page::getBlock('$block')->{'$property'}); } else { echo Page::getBlock('$block')->{'$property'}; } ?>";
            }
            return '';
        });
    }
}
