<?php

namespace App;

use App\Dimention;
use Illuminate\Database\Eloquent\Model;

class DimentionType extends Model
{
    public function dimentions() {
        return $this->hasMany(Dimention::class);
    }
}
