<?php

namespace App;

use TCG\Voyager\Traits\Spatial;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use Spatial;
    
    protected $fillable = ['contacts'];
    protected $spatial = ['coordinates'];
    protected $casts = [
        'contacts' => 'array'
    ];
}