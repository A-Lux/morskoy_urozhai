<?php

namespace App\Cart;

use App\Product;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;

class Cart implements ICart
{
    private $items = [];

    public function __construct()
    {
        if (Cookie::has('cart') && !is_null(Cookie::get('cart'))) {
            $this->items = json_decode(Cookie::get('cart'), true);
        } else {
            $this->items = [];
        }
    }

    public function add($id, $q) {
        $product = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.thumbnail',
            'products.is_weighted',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ])
        ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
        ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
        ->where('products.id', $id)
        ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.thumbnail')
        ->first()
        ->toArray();

        $product['q'] = $q;
        $this->items[$id] = $product;
        $this->updateState();
    }

    public function remove($id)
    {
        unset($this->items[$id]);
        $this->updateState();
    }

    public function count()
    {
        $count = 0;

        if($this->items) {
            $count = count($this->items);
        }
        
        return $count;
    }

    public function totalPrice() {
        $count = 0;
        if($this->items) {
            foreach($this->items as $item) {
                $count += (!$item['is_weighted'] ? ceil($item['q']) : $item['q']) * $item['price'];
            }
        }
        return $count;
    }

    public function clear()
    {
        $this->items = [];
        $this->updateState();
    }

    public function getIds() {
        return array_keys($this->items);
    }

    public function getItems() {
        return $this->items;
    }

    public function updateState()
    {
        Cookie::queue(Cookie::make('cart', json_encode($this->items), 3600));
    }
}