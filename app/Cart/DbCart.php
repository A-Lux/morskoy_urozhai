<?php

namespace App\Cart;

use App\Cart;
use App\Product;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class DbCart implements ICart {
    private $items = [];
    private $uid;

    public function __construct($uid) {
        $this->uid = $uid;
        if(Cart::where('uid',$this->uid)->exists()) {
            $this->items = Cart::select(['uid', 'items'])
                ->where('uid', $this->uid)
                ->first()
                ->toArray();
            $this->items['items'] = json_decode($this->items['items'], true);
        }else {
            $this->items = Cart::create([
                'uid' => $this->uid,
                'items' => json_encode([])
            ])
            ->toArray();

            $this->items['items'] = json_decode($this->items['items'], true);
        }
    }

    public function add($id, $q) {
        $product = Product::select(['id', 'title', 'image'])
            ->where('id', $id)
            ->first();
        $product = Product::appendAmountsToItem($product);
        $product->amount = (int)$q;
        $this->items['items'][$id] = $product;
        $this->updateState();
    }

    public function remove($id) {
        unset($this->items['items'][$id]);
        $this->updateState();
    }

    public function count() {
        $count = 0;

        foreach($this->items['items'] as $item) {
            $count += $item['amount'];
        }

        return $count;
    }

    public function totalPrice() {
        $count = 0;

        foreach($this->items['items'] as $item) {
            $count += $item['amount'] * $item['price'];
        }

        return $count;
    }

    public function clear() {
        $this->items['items'] = [];
        $this->updateState();
    }

    public function getItems() {
        return $this->items['items'];
    }

    public function updateState() {
        Cart::where('uid', $this->uid)
            ->update(['items' => json_encode($this->items['items'])]);
    }
}