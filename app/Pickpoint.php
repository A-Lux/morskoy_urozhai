<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pickpoint extends Model
{
    protected $fillable = [
        'name',
        'address',
        'phone',
        'worktime',
        'email'
    ];
}
