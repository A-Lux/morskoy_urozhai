<?php

namespace App\Viewer;

use App\Viewer\IViewer;
use Illuminate\Support\Facades\Cookie;

class Viewer implements IViewer {
    private $items = [];

    public function __construct() {
        if(Cookie::has('viewed')) {
            $this->items = json_decode(Cookie::get('viewed'), true);
        }
    }

    public function add($id) {
        if(!in_array($id, $this->items)) {
            $this->items[] = $id;
        }
        $this->updateState();
    }

    public function getItems() {
        return $this->items;
    }

    public function updateState() {
        Cookie::queue(Cookie::make('viewed', json_encode($this->items), 3600));
    }
}