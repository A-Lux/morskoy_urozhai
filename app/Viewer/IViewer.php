<?php

namespace App\Viewer;

interface IViewer {
    function add($id);
    function getItems();
}