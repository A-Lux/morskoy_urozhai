<?php

namespace App;

use App\Pickpoint;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PickupInformation extends Model
{
    protected $fillable = [
        'deliver_at', 'order_id', 'pickpoint_id'
    ];
    
    public function pickpoint() {
        return $this->belongsTo(Pickpoint::class);
    }
}
