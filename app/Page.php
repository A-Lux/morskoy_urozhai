<?php

namespace App;

use App\PageBlock;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'header',
        'url', 
        'title', 
        'description'
    ];

    public function blocks() {
        return $this->hasMany(PageBlock::class);
    }
}