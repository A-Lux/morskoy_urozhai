<?php

namespace App\View\Composers;

use App\Product;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class PopularProductsComposer {
    protected $products;

    public function __construct()
    {
        $this->products = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.thumbnail',
            'products.in_stock',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ])
        ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
        ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
        ->popular()
        ->get();
    }

    public function compose(View $view)
    {
        $view->with('popularProducts', $this->products);
    }
}
