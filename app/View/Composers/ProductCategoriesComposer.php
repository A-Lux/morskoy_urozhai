<?php

namespace App\View\Composers;

use App\Category;
use Illuminate\View\View;

class ProductCategoriesComposer {
    protected $categories;

    public function __construct()
    {
        $this->categories = Category::with('children')->ofCategoryType('App\Product')->parent()->get();
    }

    public function compose(View $view)
    {
        $view->with('popupCategories', $this->categories);
    }
}