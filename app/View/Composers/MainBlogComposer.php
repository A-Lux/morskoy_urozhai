<?php

namespace App\View\Composers;

use App\Blog;
use Illuminate\View\View;

class MainBlogComposer {
    protected $mainBlog;

    public function __construct()
    {
        $this->mainBlog = Blog::main()->first();
    }

    public function compose(View $view)
    {
        $view->with('mainBlog', $this->mainBlog);
    }
}