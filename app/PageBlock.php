<?php

namespace App;

use App\PageBlockType;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class PageBlock extends Model
{
    use Translatable;

    protected $translatable = ['content'];
    
    protected $fillable = ['slug', 'page_id', 'page_block_type_id', 'content'];

    public function type() {
        return $this->belongsTo(PageBlockType::class, 'page_block_type_id');
    }
}