<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserPromocode extends Pivot
{
    protected $fillable = ['user_id', 'promocode_id', 'used'];
}
