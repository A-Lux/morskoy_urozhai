<?php

namespace App\Traits;

use App\Filter\Filter;


trait Filterable {

	public function scopeFilter($query, Filter $filter)
	{
		return $filter->apply($this);
	}
}