<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;

class SetsLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Cookie::get('locale');
        if($locale) {
            $locale = Crypt::decrypt(Cookie::get('locale'), false);
            app()->setLocale($locale);
        }
        return $next($request);
    }
}
