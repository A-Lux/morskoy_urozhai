<?php

namespace App\Http\Middleware;

use Closure;
use App\Product;

class ChecksProductAvailability
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validated = $request->validate([
            'id' => 'required'
        ]);

        $productId = $request->id;
        if(Product::where('id', $productId)->where('in_stock', 1)->exists()) {
            return $next($request);
        }
        
        return response(['message' => 'Товара нет в наличии'], 200);
    }
}
