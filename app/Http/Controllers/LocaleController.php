<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LocaleController extends Controller
{
    public function setLocale(Request $request, $locale) {
        if(in_array($locale, config('app.locales'))) {
            Cookie::queue(Cookie::make('locale', $locale, 60));
        
            return redirect()->route('home.index');
        }
        abort(400);
    }
}
