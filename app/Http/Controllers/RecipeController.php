<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\Review;
use App\Category;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function index()
    {
        $categories = Category::ofCategoryType('App\Recipe')->get();
        $popularRecipes = Review::popularRecipes()->get();

        return view('recipes.index', compact('categories', 'popularRecipes'));
    }

    public function recipesByCategory(Category $category)
    {
        if ($category->category_type != 'App\Recipe') {
            return abort(404);
        }

        $categories = Category::ofCategoryType('App\Recipe')->get();
        $popularRecipes = Review::popularRecipes()->get();

        return view('recipes.index', compact('categories', 'popularRecipes', 'category'));
    }

    public function show(Category $category, Recipe $recipe)
    {
        $categories = Category::ofCategoryType('App\Recipe')->get();
        $popularRecipes = Review::popularRecipes()->get();

        $recipe->load('steps');
        $recipe->getProductIds();
        $rating = Recipe::selectRaw('SUM(rating) / COUNT(*) as rating')
        ->leftJoin('reviews', function ($leftJoin) {
            $leftJoin->on('reviews.reviewable_id', '=', 'recipes.id')
                ->where('reviews.reviewable_type', '=', 'App\\Recipe');
        })
        ->groupBy('reviewable_id', 'reviewable_type')->where('recipes.id', $recipe->id)->first();
        $recipe->rating = $rating->rating ? $rating->rating : 0;
        
        return view('recipes.show', compact('categories', 'popularRecipes','category', 'recipe'));
    }
}