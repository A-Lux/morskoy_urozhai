<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request) {
        $q = $request->q;
        $products = collect([]);
        
        if($q) {
            $products = Product::where('name', 'LIKE', "%$q%")->paginate(16);
        }

        return view('search', compact('products', 'q'));
    }
}
