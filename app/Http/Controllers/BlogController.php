<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index() {
        $blogs = Blog::paginate(1);
        return view('blog.index', compact('blogs'));
    }

    public function show(Blog $blog) {
        return view('blog.show', compact('blog'));
    }
}