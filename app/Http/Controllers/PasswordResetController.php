<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasswordResetController extends Controller
{
    public function index(Request $request, $token) {
        $email = $request->email;
        return view('auth.reset', compact('token', 'email'));
    }
}