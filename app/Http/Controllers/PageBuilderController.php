<?php

namespace App\Http\Controllers;

use App\PageBlockType;
use App\Page\PageBuilder;
use Illuminate\Http\Request;

class PageBuilderController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if($request->user()->isAdmin()) {
                return $next($request);
            }
        });
    }

    public function types() {
        return response(PageBlockType::select('name')->get()->pluck('name'));
    }

    public function update(Request $request) {
        $pageBuilder = new PageBuilder();
        $pageBuilder->save($request);
        
        return response(['message' => 'Updated']);
    }
}
