<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function show(Category $category)
    {
        return view('catalog', compact('category'));
    }
}
