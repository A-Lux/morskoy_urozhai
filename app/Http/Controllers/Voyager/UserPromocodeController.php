<?php

namespace App\Http\Controllers\Voyager;

use App\User;
use App\Promocode;
use App\UserPromocode;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Notifications\PromocodeAssigned;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class UserPromocodeController extends VoyagerBaseController
{
    public function store(Request $request) {
        $this->validate($request, [
            'user_id' => 'required',
            'promocode_id' => 'required'
        ]);

        if(UserPromocode::where('user_id', $request->user_id)->where('promocode_id', $request->promocode_id)->exists()) {
            return redirect()
                ->back()
                ->withErrors(['Промокод уже назначен']);
        }
        $response = parent::store($request);
        $user = User::find($request->user_id);
        $promocode = Promocode::where('id', $request->promocode_id)->first();

        $user->notify(new PromocodeAssigned($promocode, $user));
        return $response;
    }
}
