<?php

namespace App\Http\Controllers\Voyager;

use App\Blog;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class BlogController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);
        
        $blog = Blog::latest()->first();
        $blog->slug = Str::slug($blog->title);
        if($blog->is_main) {
            Blog::where('id', '<>', $blog->id)->update(['is_main' => 0]);
        }
        $blog->save();

        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);
        
        $blog = Blog::find($id);
        if($blog->is_main) {
            Blog::where('id', '<>', $blog->id)->update(['is_main' => 0]);
        }
        $blog->slug = Str::slug($blog->title);
        $blog->save();

        return $response;
    }
}
