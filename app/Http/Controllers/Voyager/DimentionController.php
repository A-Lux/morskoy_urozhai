<?php

namespace App\Http\Controllers\Voyager;

use App\Product;
use App\Dimention;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class DimentionController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);
        
        $dimention = Dimention::latest()->first();
        $dimention->slug = Str::slug($dimention->name);
        $dimention->save();

        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);

        $dimention = Dimention::find($id);
        $dimention->slug = Str::slug($dimention->name);
        $dimention->save();
        
        return $response;
    }
}
