<?php

namespace App\Http\Controllers\Voyager;

use App\Specification;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class SpecificationController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);

        $data = Specification::latest()->first();
        $data->slug = Str::slug($data->name);
        $data->save();
     
        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);

        $data = Specification::find($id);
        $data->slug = Str::slug($data->name);

        $data->save();

        return $response;
    }
}
