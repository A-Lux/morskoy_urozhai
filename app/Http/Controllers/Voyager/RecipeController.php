<?php

namespace App\Http\Controllers\Voyager;

use App\Recipe;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class RecipeController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);

        $data = Recipe::latest()->first();
        $data->slug = Str::slug($data->name);

        $data->ingredients = $request->ingredients;
        
        $data->save();
     
        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);

        $data = Recipe::find($id);
        $data->slug = Str::slug($data->name);
        $data->ingredients = $request->ingredients;
        $data->save();

        return $response;
    }
}
