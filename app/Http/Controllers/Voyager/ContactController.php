<?php

namespace App\Http\Controllers\Voyager;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ContactController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);

        $data = Contact::latest()->first();
        
        $data->contacts = $request->contacts;
        $data->save();
     
        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);

        $data = Contact::find($id);

        $data->contacts = $request->contacts;
        $data->save();

        return $response;
    }
}