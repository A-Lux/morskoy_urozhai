<?php

namespace App\Http\Controllers\Voyager;

use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class CategoryController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);

        $data = Category::latest()->first();
        $data->slug = Str::slug($data->name);
        $data->save();
        
        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);

        $data = Category::find($id);
        $data->slug = Str::slug($data->name);
        $data->save();

        return $response;
    }
}
