<?php

namespace App\Http\Controllers\Voyager;

use App\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\ProductSpecification;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ProductController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);
        
        $product = Product::latest()->first();
        $product->slug = Str::slug($product->name);

        $data = [];
        if($request->specifications) {
            foreach($request->specifications as $specification) {
                $data[] = [
                    'specification_id' => $specification['id'],
                    'value' => $specification['value'],
                    'dimention_id' => isset($specification['dimention']) ? $specification['dimention'] : null
                ];
            }
            $product->specifications()->createMany($data);
        }
        $product->save();

        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);
        
        $product = Product::find($id);
        $product->slug = Str::slug($product->name);
    
        $data = [];
        

        if($request->specifications) {
            ProductSpecification::where('product_id', $id)->delete();

            foreach($request->specifications as $specification) {
                $data[] = [
                    'specification_id' => $specification['id'],
                    'value' => $specification['value'],
                    'dimention_id' => isset($specification['dimention']) ? $specification['dimention'] : null
                ];
            }

            $product->specifications()->createMany($data);
        }
        $product->save();

        return $response;
    }
}
