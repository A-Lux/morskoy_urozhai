<?php

namespace App\Http\Controllers\Voyager;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class BillController extends VoyagerBaseController
{

    public function show(Request $request, $id) {
        $this->authorize('read', app('App\Order'));
        
        $order = Order::with('orderDetails', 'user')->findOrFail($id);
        $order->orderInformation();
        
        return view('bill', compact('order'));
    }
}
