<?php

namespace App\Http\Controllers\Voyager;

use App\Promotion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class PromotionController extends VoyagerBaseController
{
    public function store(Request $request) {
        $response = parent::store($request);
        
        $promotion = Promotion::latest()->first();
        $promotion->slug = Str::slug($promotion->name);
        $promotion->save();

        return $response;
    }

    public function update(Request $request, $id) {
        $response = parent::update($request, $id);
        
        $promotion = Promotion::find($id);
        $promotion->slug = Str::slug($promotion->name);
    
        $promotion->save();

        return $response;
    }
}
