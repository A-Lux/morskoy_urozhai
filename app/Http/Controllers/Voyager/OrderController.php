<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class OrderController extends VoyagerBaseController
{
    public function show(Request $request, $id) {    
        $response = parent::show($request, $id);
        if($response->dataTypeContent->is_viewed == false) {
            $response->dataTypeContent->is_viewed = 1;
            $response->dataTypeContent->save();
        }

        return $response;
    }
}
