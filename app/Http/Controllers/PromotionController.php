<?php

namespace App\Http\Controllers;

use App\Promotion;

class PromotionController extends Controller
{
    public function show(Promotion $promotion) {
        return view('promotion', compact('promotion'));
    }
}
