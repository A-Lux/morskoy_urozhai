<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index() {
        $banners = Banner::all();

        $offers = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.is_weighted',
            'products.thumbnail',
            'products.in_stock',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ])
        ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
        ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
        ->offers()
        ->get();

        $discounts = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.is_weighted',
            'products.thumbnail',
            'products.in_stock',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ])
        ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
        ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
        ->discount()
        ->get();

        return view('home.index', compact('banners', 'offers', 'discounts'));
    }
}
