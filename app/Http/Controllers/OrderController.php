<?php

namespace App\Http\Controllers;

use App\Order;
use App\Facades\Cart;
use App\Events\Ordered;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function success(Request $request)
    {
        $this->validate($request, [
            'pg_payment_id' => 'required'
        ]);
        
        Cart::clear();
        $order = Order::where('id', $request->pg_order_id)->firstOrFail();
        $order->pg_payment_id = $request->pg_payment_id;
        $order->save();

        event(new Ordered($order));
        return redirect('/')->with('order_success', 'Ваш заказ оформлен!');
    }

    public function failure(Request $request)
    {
        return redirect('cart')->with('order_failure', 'Неудачная попытка оплаты');
    }
}
