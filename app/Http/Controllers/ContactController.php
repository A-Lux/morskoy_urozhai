<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index() {
        $contacts = Contact::first();
        $contacts->coordinates = $contacts->getCoordinates();
        $contacts->contact = collect($contacts->contacts)->groupBy('type');

        return view('contacts', compact('contacts'));
    }
}
