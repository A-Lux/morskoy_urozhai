<?php

namespace App\Http\Controllers;

use App\Product;
use App\Facades\Viewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function index()
    {
        $products = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.is_weighted',
            'products.thumbnail',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ])
        ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
        ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
        ->whereIn('products.id', Viewer::getItems())
        ->get();
        
        return view('cart', compact('products'));
    }
}
