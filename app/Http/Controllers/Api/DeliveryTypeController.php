<?php

namespace App\Http\Controllers\Api;

use App\DeliveryType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryTypeController extends Controller
{
    public function deliveryTypess(Request $request) {
        $deliveryTypes = DeliveryType::active()->get();
        return response($deliveryTypes);
    }
}
