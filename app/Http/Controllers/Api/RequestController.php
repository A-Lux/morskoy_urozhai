<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Request as RequestModel;
use App\Http\Controllers\Controller;

class RequestController extends Controller
{
    public function request(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        
        RequestModel::create($request->all());
        return response(['message' => 'Ваша заявка принята!']);
    }
}
