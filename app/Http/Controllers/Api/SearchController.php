<?php

namespace App\Http\Controllers\Api;

use App\Recipe;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function searchWords(Request $request, $word) {
        $words = Product::select('name', 'slug', 'thumbnail')->where('name', 'LIKE', "%$word%")->get();
        if($words->count() > 0) {
            return response($words);
        }

        return response('Not Found', 404);
    }
}
