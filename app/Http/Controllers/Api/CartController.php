<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function getCart(Request $request)
    {
        $products = Cart::getItems();
        $productsArray = [];
        foreach ($products as $product) {
            $productsArray[] = $product;
        }
        return response(['products' => $productsArray, 'q' => Cart::count(), 'total' => Cart::totalPrice()]);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'q' => 'required'
        ]);

        Cart::add($request->id, $request->q);


        return response(['q' => Cart::count(), 'total' => Cart::totalPrice()], 201);
    }

    public function addGroup(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        foreach ($request->id as $id) {
            Cart::add($id, 1);
        }

        return response(['q' => Cart::count(), 'total' => Cart::totalPrice()], 201);
    }

    public function remove(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        Cart::remove($request->id);
        return response(['q' => Cart::count(), 'total' => Cart::totalPrice()]);
    }

    public function clear(Request $request)
    {
        Cart::clear();
        return response(['q' => Cart::count(), 'total' => Cart::totalPrice()]);
    }
}
