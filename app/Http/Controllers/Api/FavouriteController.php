<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Favourite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FavouriteController extends Controller
{
    public function get(Request $request)
    {
        $favourites = $request
            ->user()
            ->favourites()->select([
                'products.id',
                'products.sku',
                'products.name',
                'products.slug',
                'products.is_weighted',
                'products.thumbnail',
                'products.in_stock',
                DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
                'products.price as old_price'
            ])
            ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
            ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
            ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.thumbnail')
            ->get();

        return response($favourites);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $favouriteExists = Favourite::where('user_id', $request->user()->id)
            ->where('product_id', $request->id)
            ->exists();

        if (!$favouriteExists) {
            $request->user()->favourites()->attach($request->id);

            return response(['message' => 'Товар добавлен в избранное!']);
        }
        return response(['message' => 'Товар ранее был добавлен в избранное!']);
    }

    public function remove(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $favouriteExists = Favourite::where('user_id', $request->user()->id)
            ->where('product_id', $request->id)
            ->exists();

        if ($favouriteExists) {
            $request->user()->favourites()->detach($request->id);

            return response(['message' => 'Товар удален из избранного!']);
        }

        return response(['message' => 'Товар в избранном не найден!']);
    }
}
