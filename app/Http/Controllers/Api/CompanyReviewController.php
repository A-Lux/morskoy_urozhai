<?php

namespace App\Http\Controllers\Api;

use App\Mail\ReviewSent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CompanyReviewController extends Controller
{
    public function send(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'review' => 'required'
        ]);
        
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new ReviewSent($validatedData));
        return response(['message' => 'Спасибо за отзыв!']);
    }
}
