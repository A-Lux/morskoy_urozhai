<?php

namespace App\Http\Controllers\Api;

use App\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromocodeController extends Controller
{
    public function promocodes(Request $request) {
        $promocodes = Promocode::select(['promocodes.promocode', 'promocodes.expires_at', 'user_promocode.used'])->join('user_promocode', 'promocode_id', 'promocodes.id')
            ->where('user_promocode', $request->user()->id)
            ->get();
        
        return response($promocodes);
    }

    public function check(Request $request, $promocode)
    {
        $promocode = Promocode::where('promocode', $promocode)->first();
        $check = Promocode::check($promocode);

        switch ($check) {
            case 0:
                return response(['error' => 'Промокод не найден']);
                break;
            case 3:
                return response(['error' => 'Промокод использован']);
                break;
            case 2:
                return response(['error' => 'Промокод истек']);
                break;
            default:
                return response(['promocode' => $promocode->promocode, 'discount_percentage' => $promocode->discount_percentage]);
        }
    }
}
