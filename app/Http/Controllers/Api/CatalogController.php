<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Category;
use App\SpecificationEntity;
use Illuminate\Http\Request;
use App\ProductSpecification;
use App\Http\Controllers\Controller;

class CatalogController extends Controller
{
    public function filters(Category $category) {
        $productIds = $category->getProductIds();
        if($category->children->count() > 0) {
            $productIds = $productIds->merge($category->getChildrenProductIds());
        }

        $filters = ProductSpecification::select('specification_id', 'specifications.name as spec_name', 'specifications.slug', 'value', 'dimentions.name as dimention', 'dimentions.id as dimention_id', 'spectypes.name as spectype')
            ->whereIn('product_id', $productIds)
            ->join('specifications', 'specifications.id', 'specification_id')
            ->leftJoin('dimentions', 'dimentions.id', 'dimention_id')
            ->leftJoin('spectypes', 'spectypes.id', 'specifications.spectype_id')
            ->distinct()
            ->get();
        
        foreach($filters as $filter) {
            if($filter->spectype == 'checkbox_relation') {
                $filter->entity = SpecificationEntity::select('entity_type')->where('specification_id', $filter->specification_id)->first()->entity_type;
                $filter->entity = (new $filter->entity)->where('id', $filter->value)->first();
            }
        }

        $filters = $filters->groupBy('spec_name');
        $maxPrice = Product::selectRaw('MAX(price) as price')->first()->price;
        $filters['Категории'] = $category->children()->select(['id', 'name'])->get();
        
        return response(compact('filters', 'maxPrice'));
    }
}
