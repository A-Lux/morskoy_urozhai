<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function children(Category $category) {
        $categories = $category->children;
        
        return response($categories);
    }
}
