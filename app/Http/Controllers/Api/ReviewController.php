<?php

namespace App\Http\Controllers\Api;

use App\Recipe;
use App\Review;
use App\Product;
use App\ReviewVote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    public function product(Request $request, Product $product)
    {
        $this->validate($request, [
            'rating' => 'required|integer|min:1|max:5',
            'review' => 'required'
        ]);

        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $review = $product->reviews()->create($data);
        $review->load('user');
        $product->loadCount('reviews');
        $reviewsCount = $product->reviews_count;

        $review->loadCount(['votes as upvotes_count' => function ($query) {
            $query->where('vote', 1);
        }]);

        $review->loadCount(['votes as downvotes_count' => function ($query) {
            $query->where('vote', 0);
        }]);

        return response(compact('review', 'reviewsCount'), 200);
    }

    public function recipe(Request $request, Recipe $recipe)
    {
        $this->validate($request, [
            'rating' => 'required|integer|min:1|max:5',
            'review' => 'required'
        ]);

        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $review = $recipe->reviews()->create($data);
        $review->load('user');
        $recipe->loadCount('reviews');
        $reviewsCount = $recipe->reviews_count;

        $review->loadCount(['votes as upvotes_count' => function ($query) {
            $query->where('vote', 1);
        }]);

        $review->loadCount(['votes as downvotes_count' => function ($query) {
            $query->where('vote', 0);
        }]);

        return response(compact('review', 'reviewsCount'), 200);
    }

    public function upvote(Request $request, Review $review)
    {
        $userId = $request->user()->id;
        $reviewId = $review->id;

        if (!ReviewVote::where('user_id', $userId)->where('review_id', $reviewId)->where('vote', 1)->exists()) {
            ReviewVote::create([
                'user_id' => $userId,
                'review_id' => $reviewId,
                'vote' => 1
            ]);
            if ($downvote = ReviewVote::where('user_id', $userId)->where('review_id', $reviewId)->where('vote', 0)->first()) {
                $downvote->delete();
            }

            return response(['message' => 'upvote create'], 201);
        }

        return response(['message' => 'upvote exists'], 200);
    }

    public function downvote(Request $request, Review $review)
    {
        $userId = $request->user()->id;
        $reviewId = $review->id;

        if (!ReviewVote::where('user_id', $userId)->where('review_id', $reviewId)->where('vote', 0)->exists()) {
            ReviewVote::create([
                'user_id' => $userId,
                'review_id' => $reviewId,
                'vote' => 0
            ]);
            if ($upvote = ReviewVote::where('user_id', $userId)->where('review_id', $reviewId)->where('vote', 1)->first()) {
                $upvote->delete();
            }

            return response(['message' => 'downvote create'], 201);
        }

        return response(['message' => 'downvote exists'], 200);
    }
}
