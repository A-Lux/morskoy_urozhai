<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Dimention;
use App\SpecificationEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function specifications(Product $product) {
        $specifications = $product->specifications()
            ->select(['specifications.id', 'specifications.name', 'value', 'dimentions.name as dimention', 'specifications.dimention_type_id', 'dimention_id'])
            ->join('specifications', 'specifications.id', 'specification_id')
            ->leftJoin('dimentions', 'dimentions.id', 'dimention_id')
            ->get();

        foreach($specifications as $specification) {
            $specification->entities = SpecificationEntity::select('entity_type')->where('specification_id', $specification->id)->first();
            $specification->dimentions = $specification->dimention_type_id ? Dimention::select('id', 'name')->where('dimention_type_id', $specification->dimention_type_id)->get() : null;
            if($specification->entities) {
                $specification->entities = (new $specification->entities->entity_type)->get();
            }
        }

        return response($specifications);
    }
    
    public function reviews(Product $product) {
        $reviews = $product->reviews()
            ->with(['user' => function($q) {
                $q->select('id', 'name');
            }])
            ->withCount(['votes as upvotes_count' => function ($query) {
                $query->where('vote', 1);
            }])->withCount(['votes as downvotes_count' => function ($query) {
                $query->where('vote', 0);
            }])->latest()->paginate(6);

        return response($reviews);
    }
}
