<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function user(Request $request)
    {
        $user = $request
            ->user()
            ->load('addresses');

        return response()->json($user);
    }

    public function getActiveAddress(Request $request)
    {
        return response(
            $request
                ->user()
                ->addresses()
                ->active()
                ->first()
        );
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'surname' => 'required|string|min:3',
            'sex' => 'nullable',
            'birthday' => 'nullable|date',
            'password' => 'nullable|string|min:6|confirmed',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'email' => 'required|email|unique:users,email,' . $request->user()->id,
            'avatar' => 'image'
        ]);

        $addresses = collect($request->addresses);
        $isActive = $request->is_active;
        $userData = $request->except('addresses', 'avatar', 'password_confirmation');
        if ($request->password) {
            $userData['password'] = Hash::make($request->password);
        }

        if ($request->hasFile('avatar')) {
            $userData['avatar'] = $request->file('avatar')->store('avatars', 'public');
        } else {
            $userData['avatar'] = $request->user()->avatar;
        }

        $request->user()->addresses()->delete();

        $addresses = $addresses->map(function ($address, $index) use ($isActive) {
            if ($index == $isActive) {
                $address['is_active'] = 1;
            } else {
                $address['is_active'] = 0;
            }

            return $address;
        });

        $request->user()->addresses()->createMany($addresses->toArray());

        $request->user()->update($userData);
        return response()->noContent();
    }

    public function orders(Request $request)
    {
        $orders = $request->user()->orders()->with('orderStatus')->get();


        return response($orders);
    }

    public function order(Request $request, Order $order)
    {
        $orderDetails = OrderDetail::select(['products.id', 'products.slug', 'products.name', 'products.thumbnail', 'unit_price', 'unit_quantity'])
            ->where('order_id', $order->id)
            ->join('products', 'products.id', 'product_id')
            ->get();

        return response($orderDetails);
    }
}
