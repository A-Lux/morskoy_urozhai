<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Category;
use App\Promotion;
use Illuminate\Http\Request;
use App\Filter\ProductFilter;
use App\ProductSpecification;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Filter\ProductSpecificationFilter;

class FilterController extends Controller
{
    public function filter(Request $request, ProductSpecificationFilter $filter, ProductFilter $productFilter)
    {
        $productIds = ProductSpecification::filter($filter);
        $products = Product::filter($productFilter)
            ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
            ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
            ->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
            ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.discount', 'products.thumbnail');

        if ($request->price) {
            $products = $products->price(['calculatePrice' => Product::calculatePrice('promotions.discount_percentage'), 'priceRange' => $request->price]);
        }

        if ($productIds != false) {
            $products = $products->whereIn('products.id', $productIds);
        }

        if ($request->sort) {
            $products = $products->sort(['sort' => $request->sort, 'calculatePrice' => Product::calculatePrice('promotions.discount_percentage')]);
        }

        if ($request->category) {
            $categories = Category::whereIn('id', $request->category)->get();
            $categoryIds = collect([]);
            foreach($categories as $category) {
                $categoryIds->push($category->id);
                $categoryIds = $categoryIds->merge($category->getChildrenIds());
            }

            $products = $products->whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn('categories.id', $categoryIds);
            });
        }

        $products = $products->paginate(32, [
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.is_weighted',
            'products.thumbnail',
            'products.in_stock',
            DB::raw('SUM(rating) / COUNT(*) as rating'),
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price'
        ]);

        return response($products);
    }

    public function filterPromotions(Request $request, Promotion $promotion, ProductSpecificationFilter $filter, ProductFilter $productFilter)
    {
        $productIds = ProductSpecification::filter($filter);
        $discountPercentage = $promotion->discount_percentage;
        $calculatePrice = Product::calculatePrice($discountPercentage);
        $products = $promotion->products()
            ->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
            ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.thumbnail');

        if ($request->price) {
            $products = $products->price(['calculatePrice' => $calculatePrice, 'priceRange' => $request->price]);
        }

        if ($productIds != false) {
            $products = $products->whereIn('products.id', $productIds);
        }

        if ($request->sort) {
            $products = $products->sort(['sort' => $request->sort, 'calculatePrice' => $calculatePrice]);
        }

        if ($request->category) {
            $categories = $request->category;
            $products = $products->whereHas('categories', function ($query) use ($categories) {
                $query->whereIn('categories.id', $categories);
            });
        }

        $products = $products->paginate(16, [
            'products.id',
            'products.sku',
            'products.name',
            'products.slug',
            'products.thumbnail',
            DB::raw('SUM(rating) / COUNT(*) as rating'),
            'products.price as old_price',
            DB::raw("$calculatePrice AS price")
        ]);

        return response($products);
    }
}
