<?php

namespace App\Http\Controllers\Api;

use App\Recipe;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeController extends Controller
{
    public function recipes()
    {
        $recipes = Recipe::with(['category' => function($q) {
            return $q->select(['id', 'slug']);
        }])->paginate(4);
        return response($recipes);
    }

    public function recipesByCategory(Category $category)
    {
        if ($category->category_type != 'App\Recipe') {
            return response(['message' => 'Category not found'], 404);
        }

        $recipes = $category
            ->recipes()
            ->paginate(16);

        return response($recipes);
    }

    public function reviews(Recipe $recipe)
    {
        $reviews = $recipe->reviews()
            ->with(['user' => function ($q) {
                $q->select('id', 'name');
            }])
            ->withCount(['votes as upvotes_count' => function ($query) {
                $query->where('vote', 1);
            }])->withCount(['votes as downvotes_count' => function ($query) {
                $query->where('vote', 0);
            }])->latest()->paginate(6);

        return response($reviews);
    }
}
