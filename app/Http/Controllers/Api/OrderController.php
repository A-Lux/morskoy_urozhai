<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\Product;
use App\Pickpoint;
use App\Promocode;
use App\OrderStatus;
use App\PaymentType;
use App\DeliveryType;
use App\Facades\Cart;
use App\UserPromocode;
use App\Events\Ordered;
use App\Facades\Paybox;
use App\Order\OrderMaker;
use App\Order\OrderBuilder;
use Illuminate\Support\Str;
use App\Order\DeliveryMaker;
use App\Order\PickupBuilder;
use Illuminate\Http\Request;
use App\Order\DeliveryBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function store(Request $request, DeliveryBuilder $deliveryBuilder, PickupBuilder $pickupBuilder, OrderBuilder $orderBuilder)
    {
        $this->validate($request, [
            'email' => 'required',
            'street' => 'required_if:delivery_type,1',
            'pickpoint' => 'required_if:delivery_type,2',
            'phone' => 'required',
            'house' => 'required_if:delivery_type,1',
            'floor' => 'nullable',
            'apartments' => 'nullable',
            'payment_type' => 'required|integer',
            'delivery_type' => 'required|integer',
            'deliver_at' => 'required',
            'house_building' => 'nullable',
            'comment' => 'nullable',
            'promocode' => 'nullable',
            'porch' => 'nullable',
            'office' => 'nullable',
            'products' => 'json'
        ]);

        $newProducts = collect(json_decode($request->products));
        $paymentType = PaymentType::where('id', $request->payment_type)->first();
        $deliveryType = DeliveryType::where('id', $request->delivery_type)->first();

        if ($paymentType && $deliveryType) {
            $orderData = [];
            $deliveryData = [];
            $promocodePercentage = null;
            $totalPrice = Cart::totalPrice();

            $orderData['paymentType'] = $paymentType;
            $orderData['deliveryType'] = $deliveryType;
            $orderData['phone'] = $request->phone;
            $orderData['comment'] = $request->comment;
            $orderData['orderStatus'] = OrderStatus::select('id')->where('order', 1)->first();
            $orderData['email'] = $request->email;

            if (Auth::guard('api')->check()) {
                if ($request->promocode) {
                    $promocode = Promocode::where('promocode', $request->promocode)->first();
                    if (Promocode::check($promocode)) {
                        $promocodePercentage = $promocode->discount_percentage;
                        $totalPrice = $totalPrice - ($totalPrice * $promocodePercentage / 100);
                        UserPromocode::where('user_id', Auth::guard('api')->user()->id)->where('promocode_id', $promocode->id)
                            ->update(['used' => 1]);

                        $orderData['promocode'] = $promocode;
                    }
                }
                $orderData['user'] = Auth::guard('api')->user();
            }

            $order = OrderMaker::makeOrder($orderBuilder, $orderData);
            $order->save();

            if ($order->delivery_type == 1) {
                $deliveryData['house'] = $request->house;
                $deliveryData['street'] = $request->street;
                $deliveryData['houseBuilding'] = $request->house_building;
                $deliveryData['porch'] = $request->porch;
                $deliveryData['floor'] = $request->floor;
                $deliveryData['apartments'] = $request->apartments;
                $deliveryData['office'] = $request->office;
                $deliveryData['deliverAt'] = $request->deliver_at;
                $deliveryData['order'] = $order;

                $delivery = DeliveryMaker::makeDelivery($deliveryBuilder, $deliveryData);
            } else {
                $deliveryData['pickpoint'] = Pickpoint::find($request->pickpoint);
                $deliveryData['deliverAt'] = $request->deliver_at;
                $deliveryData['order'] = $order;

                $delivery = DeliveryMaker::makePickup($pickupBuilder, $deliveryData);
            }
            $delivery->save();

            $products = Cart::getItems();
            $productsData = [];
            $productIds = [];
            $description = "";

            foreach ($newProducts as $productTemp) {
                $product = Product::find($productTemp->id);
                if ($promocodePercentage) {
                    $product['price'] = $product['price'] - ($product['price'] * $promocodePercentage / 100);
                }

                $productsData[] = [
                    'product_id' => $product['id'],
                    'unit_price' => $product['price'],
                    'unit_quantity' => $productTemp->quantity->amount,
                ];

                $productIds[] = $product['id'];
            }

            $order->orderDetails()->createMany($productsData);
            if ($paymentType->id == 1) {
                $products = Product::select('name')->whereIn('id', $productIds)->get();

                foreach ($products as $product) {
                    $description .= $product->name . " ";
                }

                Paybox::setPgMerchantId(env('PAYBOX_MERCHANT_ID'));
                Paybox::setPgAmount($totalPrice);
                Paybox::setPgSalt(Str::random(15));
                Paybox::setPgOrderId($order->id);
                Paybox::setPgDescription($description);
                Paybox::setPgFailureUrl(route('order.failure'));
                Paybox::setPgSuccessUrl(route('order.success'));

                return response(['payment_url' => Paybox::pay()]);
            } else {
                Cart::clear();
                event(new Ordered($order));
                return response(['message' => 'Заказ принят в обработку!']);
            }
        } else {
            return response(['message' => 'Неверный метод оплаты или доставки!'], 404);
        }
    }
}
