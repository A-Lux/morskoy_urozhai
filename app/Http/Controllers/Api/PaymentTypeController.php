<?php

namespace App\Http\Controllers\Api;

use App\DeliveryType;
use App\PaymentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pickpoint;

class PaymentTypeController extends Controller
{
    public function paymentTypes(Request $request)
    {
        $paymentType = PaymentType::active()->get();
        return response($paymentType);
    }

    public function deliveryTypes(Request $request)
    {
        $deliveryTypes = DeliveryType::active()->get();
        return response($deliveryTypes);
    }

    public function pickpoints(Request $request)
    {
        $pickpoints = Pickpoint::get();
        return response($pickpoints);
    }
}
