<?php

namespace App\Http\Controllers\Api;

use App\Dimention;
use App\Specification;
use App\SpecificationEntity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpecificationController extends Controller
{
    public function index() {
        $specifications = Specification::select('specifications.id', 'specifications.name', 'spectypes.name as spec_name', 'specifications.dimention_type_id')
            ->join('spectypes', 'spectypes.id', 'specifications.spectype_id')
            ->get();

        foreach($specifications as $specification) {
            $specification->entities = SpecificationEntity::select('entity_type')->where('specification_id', $specification->id)->first();
            $specification->dimentions = Dimention::select('id', 'name')->where('dimention_type_id', $specification->dimention_type_id)->get();
            if($specification->entities) {
                $specification->entities = (new $specification->entities->entity_type)->get();
            }
        }
        
        return response($specifications);
    }

    public function specification(Specification $specification) {
        $specification->entities = SpecificationEntity::select('entity_type')->where('specification_id', $specification->id)->first();
        $specification->dimentions = Dimention::select('id', 'name')->where('dimention_type_id', $specification->dimention_type_id)->get();
        if($specification->entities) {
            $specification->entities = (new $specification->entities->entity_type)->get();
        }

        return response($specification);
    }

    public function dimentions(Specification $specification) {
        return response($specification->dimentionType->dimentions);
    }
}
