<?php

namespace App\Http\Controllers;

use App\Review;
use App\Product;
use App\Facades\Viewer;
use App\SpecificationEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function show(Product $product)
    {
        $product = Product::select([
            'products.id',
            'products.sku',
            'products.name',
            'products.description',
            'products.slug',
            'products.is_weighted',
            'products.thumbnail',
            'products.slider_images',
            DB::raw(Product::calculatePrice('promotions.discount_percentage') . " AS price"),
            'products.price as old_price',
            'products.in_stock'
        ])
            ->leftJoin('product_promotion', 'product_promotion.product_id', 'products.id')
            ->leftJoin('promotions', 'promotions.id', 'product_promotion.promotion_id')
            ->where('products.id', $product->id)
            ->first();

        $specifications = $product->specifications()
            ->select(['specifications.id', 'specifications.name', 'spectypes.name as spectype', 'dimentions.name as dimention', 'value'])
            ->leftJoin('specifications', 'specifications.id', 'product_specification.specification_id')
            ->leftJoin('spectypes', 'spectypes.id', 'specifications.spectype_id')
            ->leftJoin('dimentions', 'dimentions.id', 'product_specification.dimention_id')
            ->get();

        foreach ($specifications as $specification) {
            $specification->entity = SpecificationEntity::select('entity_type')->where('specification_id', $specification->id)->first();
            if ($specification->entity) {
                $specification->entity = (new $specification->entity->entity_type)->where('id', $specification->value)->first();
            }
        }

        $rating = Product::select(DB::raw('SUM(rating) / COUNT(*) as rating'))
            ->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
            ->groupBy('reviewable_id', 'reviewable_type')->where('products.id', $product->id)->first();

        $product->rating = $rating->rating ? $rating->rating : 0;
        $product->loadCount('reviews');
        Viewer::add($product->id);

        return view('products.index', compact('product', 'specifications'));
    }
}
