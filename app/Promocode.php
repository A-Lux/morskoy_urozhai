<?php

namespace App;

use App\UserPromocode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    private static $NOT_FOUND = 0;
    private static $USED = 3;
    private static $EXPIRED = 2;

    static public function check($promocode)
    {
        if (!$promocode) {
            return static::$NOT_FOUND;
        }
        if ($promocode->expires_at <= date('Y-m-d')) {
            return static::$EXPIRED;
        }

        if (UserPromocode::where('user_id', Auth::guard('api')->user()->id)->where('promocode_id', $promocode->id)->where('used', 1)->exists()) {
            return static::$USED;
        }
        
        return $promocode;
    }
}