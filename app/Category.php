<?php

namespace App;

use App\Recipe;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['slug'];
    protected $hidden = ['pivot'];

    public function products() {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public function getProductIds() {
        return $this->products()
            ->select('products.id')
            ->get()
            ->pluck('id');
    }

    public function getChildrenProductIds() {
        $productIds = collect([]);
        foreach($this->children as $child) {
            $productIds = $productIds->merge($child->getProductIds());
        }
        return $productIds;
    }

    public function getChildrenIds() {
        return $this->children()
            ->select('categories.id')
            ->get()
            ->pluck('id');
    }

    public function recipes() {
        return $this->hasMany(Recipe::class);
    }

    public function scopeOfCategoryType($query, $type)
    {
        return $query->where('category_type', $type);
    }

    public function children() {
        return $this->hasMany(static::class);
    }

    public function scopeParent($query) {
        return $query->whereNull('category_id');
    }
}
