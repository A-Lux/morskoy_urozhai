<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DeliveryInformation extends Model
{
    protected $fillable = ['street', 'house_building', 'porch', 'floor', 'house', 'apartments', 'office', 'deliver_at', 'order_id'];
}
