<?php

namespace App;

use App\Review;
use App\Category;
use App\Promotion;
use App\Traits\Filterable;
use Illuminate\Support\Str;
use App\ProductSpecification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Filterable;
    protected $hidden = ['pivot'];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function promotions()
    {
        return $this->belongsToMany(Promotion::class, 'product_promotion');
    }

    public function specifications()
    {
        return $this->hasMany(ProductSpecification::class);
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    static public function scopePopular($query)
    {
        $products = collect([]);

        if (setting('real_popular') && setting('real_popular') == true) {
            $products = $query->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
                ->orderBy(DB::raw('SUM(rating) / COUNT(*)'), 'DESC')
                ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.thumbnail');
        } else {
            $products = $query->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
                ->groupBy('products.id', 'products.sku', 'products.name', 'products.slug', 'products.price', 'products.thumbnail')
                ->where('is_popular', '<>', 0);
        }

        return $products;
    }

    static public function sortable()
    {
        return collect(['rating', 'price']);
    }

    public function scopeDiscount($query)
    {
        return $query->whereNotNull('discount');
    }

    public function scopeOffers($query)
    {
        return $query->where('is_offer', 1);
    }

    public function scopeWithRating($query)
    {
        return $query->selectRaw('reviewable_id, reviewable_type, SUM(rating) / COUNT(*) as rating')
            ->leftJoin('reviews', function ($leftJoin) {
                $leftJoin->on('reviews.reviewable_id', '=', 'products.id')
                    ->where('reviews.reviewable_type', '=', 'App\\Product');
            })
            ->groupBy('reviewable_id', 'reviewable_type');
    }

    public function getDiscountPercentage($price, $discount)
    {
        if ($discount) {
            return ceil((1 - ($discount / $price)) * -100);
        }
        return 0;
    }

    public function scopePrice($query, $params)
    {

        $value = explode('_', $params['priceRange']);
        $value = array_filter($value);
        $min = $value[0];
        if (isset($value[1])) {
            $max = $value[1];
        } else {
            $max = static::selectRaw('MAX(price) as price')->first()->price;
        }

        return $query->where(DB::raw($params['calculatePrice']), '>=', $min)->where(DB::raw($params['calculatePrice']), '<=', $max);
    }

    public function scopeSort($query, $params)
    {
        $sort = explode('_', $params['sort']);
        $sort = array_filter($sort);
        if (static::sortable()->contains($sort[0])) {
            if ($sort[0] == 'price') {
                return $query->orderBy(DB::raw($params['calculatePrice']), isset($sort[1]) && $sort[1] > 0  ? 'asc' : 'desc');
            }
            return $query->orderBy($sort[0], isset($sort[1]) && $sort[1] > 0  ? 'asc' : 'desc');
        }

        return $query;
    }

    static public function calculatePrice($discountPercentage)
    {
        return "CalculatePrice($discountPercentage,
            CASE
                WHEN products.discount IS NOT NULL THEN products.discount
                ELSE products.price
            END)";
    }

    public function getShortDescription()
    {
        return Str::limit(strip_tags($this->description), 75, '...');
    }
}
