<?php

namespace App;

use App\User;
use App\OrderDetail;
use App\OrderStatus;
use App\PaymentType;
use App\DeliveryType;
use App\PickupInformation;
use App\DeliveryInformation;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["user_id", 'email', 'phone', "comment", "payment_type", "order_status_id", "pg_payment_id", "dilery_type", "promocode_id"];

    public function orderDetails() {
        return $this->hasMany(OrderDetail::class);
    }

    public function orderStatus() {
        return $this->belongsTo(OrderStatus::class);
    }

    public function orderInformation() {
        switch($this->delivery_type) {
            case 1:
                return $this->delivery;
            case 2:
                if($this->pickup) {
                    $this->pickup->load('pickpoint');
                }

                return $this->pickup;
            default:
                return $this->delivery;
        }
    }

    public function pickup() {
        return $this->hasOne(PickupInformation::class);
    }

    public function delivery() {
        return $this->hasOne(DeliveryInformation::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function paymentType() {
        return $this->belongsTo(PaymentType::class, 'payment_type', 'id');
    }

    public function deliveryType() {
        return $this->belongsTo(DeliveryType::class, 'delivery_type', 'id');
    }
}