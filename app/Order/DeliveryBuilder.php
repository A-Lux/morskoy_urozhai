<?php

namespace App\Order;

use App\Order;
use Carbon\Carbon;
use App\DeliveryInformation;

class DeliveryBuilder {
    private $delivery;

    public function reset() {
        $this->delivery = new DeliveryInformation();
    }

    public function setStreet($street) {
        $this->delivery->street = $street;
    }

    public function setHouse($house) {
        $this->delivery->house = $house;
    }

    public function setHouseBuilding($houseBuilding) {
        $this->delivery->house_building = $houseBuilding;
    }

    public function setPorch($porch) {
        $this->delivery->porch = $porch;
    }

    public function setFloor($floor) {
        $this->delivery->floor = $floor;
    }

    public function setApartments($apartments) {
        $this->delivery->apartments = $apartments;
    }

    public function setOffice($office) {
        $this->delivery->office = $office;
    }

    public function setDeliverAt(string $deliverAt) {
        $this->delivery->deliver_at = Carbon::createFromFormat('Y-m-d H:i:s', $deliverAt);;
    }

    public function applyTo(Order $order) {
        $this->delivery->order_id = $order->id;
    }

    public function getDelivery() {
        return $this->delivery;
    }
}