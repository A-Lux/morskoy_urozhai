<?php

namespace App\Order;

use App\Order\OrderBuilder;

class OrderMaker {
    static public function makeOrder(OrderBuilder $builder, array $data) {
        $builder->reset();
        $builder->setUser(isset($data['user']) ? $data['user'] : null);
        $builder->setEmail($data['email']);
        $builder->applyPromocode(isset($data['promocode']) ? $data['promocode'] : null);
        $builder->setPaymentType($data['paymentType']);
        $builder->setDeliveryType($data['deliveryType']);
        $builder->setPhone($data['phone']);
        $builder->setComment($data['comment']);
        $builder->setOrderStatus($data['orderStatus']);
        return $builder->getOrder();
    }
}