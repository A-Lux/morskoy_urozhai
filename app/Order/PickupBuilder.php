<?php

namespace App\Order;

use App\Order;
use App\Pickpoint;
use Carbon\Carbon;
use App\PickupInformation;

class PickupBuilder {
    private $delivery;

    public function reset() {
        $this->delivery = new PickupInformation();
    }

    public function setPickpoint(Pickpoint $pickpoint) {
        $this->delivery->pickpoint_id = $pickpoint->id;
    }

    public function setDeliverAt(string $deliverAt) {
        $this->delivery->deliver_at = Carbon::createFromFormat('Y-m-d H:i:s', $deliverAt);
    }

    public function applyTo(Order $order) {
        $this->delivery->order_id = $order->id;
    }

    public function getDelivery() {
        return $this->delivery;
    }
}