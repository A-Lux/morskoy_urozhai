<?php

namespace App\Order;

use App\Order\PickupBuilder;
use App\Order\DeliveryBuilder;
/**
 * if DeliveryType = delivery
 * DeliveryInformation: (id, street, house_building, porch, floor, house, apartments, office, delivery_time)
 * else
 * Pickpoint: (id, name, address)
 * PickpointInformation (id, pickup_time, pickpoint_id)
*/
class DeliveryMaker {
    static public function makeDelivery(DeliveryBuilder $builder, array $data) {
        $builder->reset();
        $builder->setHouse($data['house']);
        $builder->setStreet(isset($data['street']) ? $data['street'] : null);
        $builder->setHouseBuilding(isset($data['houseBuilding']) ? $data['houseBuilding'] : null);
        $builder->setPorch(isset($data['porch']) ? $data['porch'] : null);
        $builder->setFloor(isset($data['floor']) ? $data['floor'] : null);
        $builder->setApartments(isset($data['apartments']) ? $data['apartments'] : null);
        $builder->setOffice(isset($data['office']) ? $data['office'] : null);
        $builder->setDeliverAt($data['deliverAt']);
        $builder->applyTo($data['order']);
        return $builder->getDelivery();
    }

    static public function makePickup(PickupBuilder $builder, array $data) {
        $builder->reset();
        $builder->setPickpoint($data['pickpoint']);
        $builder->setDeliverAt($data['deliverAt']);
        $builder->applyTo($data['order']);
        return $builder->getDelivery();
    }
}