<?php

namespace App\Order;

use App\User;
use App\Order;
use App\Promocode;
use App\OrderStatus;
use App\PaymentType;
use App\DeliveryType;
use App\Order\IOrderBuilder;

class OrderBuilder
{
    private $order;

    public function reset()
    {
        $this->order = new Order();
    }

    public function setUser($user)
    {
        if ($user) {
            $this->order->user_id = $user->id;
        }
    }

    public function setEmail($email)
    {
        $this->order->email = $email;
    }

    public function setPhone(string $phone)
    {
        $this->order->phone = $phone;
    }

    public function setComment($comment)
    {
        $this->order->comment = $comment;
    }

    public function setOrderStatus(OrderStatus $orderStatus)
    {
        $this->order->order_status_id = $orderStatus->id;
    }

    public function setPaymentType(PaymentType $paymentType)
    {
        $this->order->payment_type = $paymentType->id;
    }

    public function setDeliveryType(DeliveryType $deliveryType)
    {
        $this->order->delivery_type = $deliveryType->id;
    }

    public function applyPromocode($promocode)
    {
        if ($promocode) {
            $this->order->promocode_id = $promocode->id;
        }
    }

    public function getOrder()
    {
        return $this->order;
    }
}
