<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Paybox extends Facade {

    protected static function getFacadeAccessor() { return 'paybox'; }
}