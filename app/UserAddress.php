<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = ['street', 'house_building', 'porch', 'floor', 'house', 'apartments', 'office', 'is_active'];

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}
 