<?php

namespace App;

use App\Dimention;
use App\Specification;
use App\Traits\Filterable;
use App\SpecificationEntity;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductSpecification extends Pivot
{
    use Filterable;
    
    protected $fillable = ['product_id', 'specification_id', 'value', 'dimention_id'];

    public function specification() {
        return $this->belongsTo(Specification::class);
    }
    
    public function dimention() {
        return $this->belongsTo(Dimention::class);
    }
}
