<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ["product_id", "order_id", "unit_price", "unit_quantity"];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
