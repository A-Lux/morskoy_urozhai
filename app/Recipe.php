<?php

namespace App;

use App\Product;
use App\Category;
use App\RecipeStep;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $casts = [
        'ingredients' => 'array'
    ];
    protected $fillable = ['ingredients'];

    public function steps() {
        return $this->hasMany(RecipeStep::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public function getProductIds() {
        $this->products = $this->products()
            ->select('products.id')
            ->get()
            ->pluck('id');
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
    
    public function getShortDescription() {
        return Str::limit(strip_tags($this->description), 75, '...');
    }
}