<?php

namespace App\Filter;

use App\Filter\Filter;
use Illuminate\Support\Str;

class ProductFilter extends Filter {
    public $filters = [];

    public function price($builder, $value) {
        $value = explode('_', $value['value']);
        $value = array_filter($value);
        $min = $value[0];
        if(isset($value[1])) {
            $max = $value[1];
        } else {
            $max = 99999;
        }

        return $builder->where('price', '>=', $min)->where('price', '<=', $max);
    }

    public function loadFilters() {
        $this->filters = [
            'price' => ['type' => 'price'],
        ];
    }

    public function apply($builder)
	{
        foreach ($this->filters() as $filter => $value) {
			$method = Str::camel($this->filters[$filter]['type']);
			if (method_exists($this, $method)) {
			    $builder = $this->applyFilter($builder, $method, $value, $this->filters[$filter]);
            }
        }
        
        return $builder;
	}
}