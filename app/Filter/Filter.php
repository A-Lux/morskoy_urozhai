<?php

namespace App\Filter;

use App\FilterProduct;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

abstract class Filter {

	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->loadFilters();
	}

	public function apply($builder)
	{
		$ids = [];
		if (count($this->filters()) == 0) {
			return false;
		}

		foreach ($this->filters() as $filter => $value) {
			$method = Str::camel($this->filters[$filter]['type']);
			if (method_exists($this, $method)) {
				$ids[] = $this->applyFilter(new $builder, $method, $value, $this->filters[$filter])
					->select('product_id')
					->get()
					->pluck('product_id')
					->toArray();
			}
		}

		if(count($this->filters()) > 0) {
			if(count($ids) > 1) {
				$ids = array_intersect(...$ids);
			} else {
				$ids = $ids[0];
			}
		}

		return $ids;
	}

	public function filters()
	{
		return array_filter($this->request->only(array_keys($this->filters)));
	}

	abstract protected function loadFilters();

	protected function applyFilter($builder, $method, $value, $params) {
		$method = Str::camel($method);
		return $this->$method($builder, ['value' => $value, 'params' => $params]);
	}
}