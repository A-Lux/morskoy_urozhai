<?php

namespace App;

use App\User;
use App\ReviewVote;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['user_id', 'review', 'rating'];
    protected $hidden = ['reviewable_id', 'reviewable_type'];
    protected $casts = [
        'created_at' => 'datetime:d.m.Y'
    ];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function scopePopularRecipes($query)
    {
        return $query->selectRaw('reviewable_id, reviewable_type, SUM(rating) / COUNT(rating) as rate')
            ->with('reviewable')
            ->where('reviewable_type', 'App\Recipe')
            ->whereRaw('DATEDIFF(CURDATE(), DATE_FORMAT(created_at,"%Y-%m-%d")) <= 7')
            ->groupBy('reviewable_id', 'reviewable_type')
            ->orderBy('rate', 'DESC')
            ->limit(3);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function votes() {
        return $this->hasMany(ReviewVote::class);
    }
}
