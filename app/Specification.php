<?php

namespace App;

use App\DimentionType;
use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    public function dimentionType() {
        return $this->belongsTo(DimentionType::class);
    }
}
