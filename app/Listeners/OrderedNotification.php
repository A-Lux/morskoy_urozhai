<?php

namespace App\Listeners;

use PDF;
use App\Events\Ordered;
use App\Notifications\InvoicePaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class OrderedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Ordered $event)
    {
        $event->order->orderInformation();
        
        $path = storage_path('app/private') . '/order' . $event->order->id . '.pdf';
        $subject = 'Чек на заказ №' . $event->order->id;
        $pdf = PDF::setOptions(['dpi' => 100, 'defaultFont' => 'arial'])
            ->loadView('pdf.invoice', ['order' => $event->order]);
        $pdf->save($path);
        
        $emails = [];
        $emails[] = setting('site.ownerEmail');
        $emails[] = $event->order->email;

        Notification::route('mail', $emails)
                ->notify(new InvoicePaid($path, $subject));
    }
}
