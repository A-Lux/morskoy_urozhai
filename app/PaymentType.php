<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $fillable = ['name', 'is_active'];
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}
