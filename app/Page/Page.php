<?php

namespace App\Page;

class Page {
    private $title = '';
    private $description = '';
    private $blocks;

    public function setTitle(string $title) : void {
        $this->title = $title;
    }
    
    public function setBlocks($blocks) : void {
        $this->blocks = $blocks->groupBy('slug');
    }

    public function setDescription(string $description) : void {
        $this->description = $description;
    }

    public function getTitle() : string {
        return $this->title;
    }

    public function getDescription() : string {
        return $this->description;
    }

    public function getBlocks() {
        return $this->blocks;
    }

    public function getBlock($name) 
    {
        if (isset($name, $this->blocks)) {
            if(config('voyager.multilingual.enabled')) {
                $this->blocks[$name] = $this->blocks[$name]->translate(app()->getLocale());
            }
            return $this->blocks[$name][0];
        }
        
        return null;
    }
}