<?php

namespace App\Page;

use App\PageBlock;
use App\PageBlockType;

class PageBuilder {
    private $types = null;
    
    public function __construct() {
        $this->types = PageBlockType::select('name', 'option')->get()->groupBy('name');
    }

    public function save($data) {
        if($this->typeExists($data->type)) {
            $option = $this->types[$data->type]->first()->option;
            switch($option) {
                case 'text':
                    return $this->saveText($data);
                break;
                case 'image':
                    return $this->saveImage($data);
                break;
            }
        }
    }

    private function typeExists($type) {
        return $this->types->has($type);
    }

    private function saveText($data) {
        if(config('voyager.multilingual.enabled')) {
            if(app()->isLocale(config('app.fallback_locale'))) {
                return $this->saveTextWithoutTranslation($data);
            }

            return $this->saveTextWithTranslation($data);
        }

        return $this->saveTextWithoutTranslation($data);
    }

    private function saveTextWithoutTranslation($data) {
        return PageBlock::where('slug', $data->slug)->update(['content' => $data->content]);
    }

    private function saveTextWithTranslation($data) {
        $pageBlock = PageBlock::where('slug', $data->slug)->first();
        $pageBlock->translations()->updateOrCreate(
            [
                'table_name' => $pageBlock->getTable(),
                'column_name' => 'content',
                'locale' => app()->getLocale()
            ],
            [
                'table_name' => $pageBlock->getTable(),
                'column_name' => 'content',
                'locale' => app()->getLocale(),
                'value' => $data->content
            ]
        );

        return $pageBlock;
    }

    private function saveImage($data) {
        $path = $data->file('content')->store('page_blocks/' . $data->slug, 'public');
        return PageBlock::where('slug', $data->slug)->update(['content' => $path]);
    }
}