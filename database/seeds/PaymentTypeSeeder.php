<?php

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::insert([
            [
                'name' => 'Онлайн оплата',
                'is_active' => 0
            ],
            [
                'name' => 'Самовывоз',
                'is_active' => 1
            ],
            [
                'name' => 'Картой при получении (терминал)',
                'is_active' => 0
            ],
            [
                'name' => 'Картой при получении (перевод)',
                'is_active' => 1
            ],
            [
                'name' => 'Наличными при получении',
                'is_active' => 1
            ]
        ]);
    }
}
