<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DimentionTypeSeeder::class,
            DimentionSeeder::class,
            SpectypeSeeder::class,
            SpecificationSeeder::class,
            DeliveryTypeSeeder::class,
            PaymentTypeSeeder::class,
            PickpointSeeder::class,
            OrderStatusSeeder::class,
            PageSeeder::class,
            PageBlockTypeSeeder::class,
            PageBlockSeeder::class
        ]);
    }
}
