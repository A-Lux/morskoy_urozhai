<?php

use App\PageBlockType;
use Illuminate\Database\Seeder;

class PageBlockTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PageBlockType::insert([
            ['name' => 'Txt', 'option' => 'text'],
            ['name' => 'Img', 'option' => 'image'],
            ['name' => 'BgImg', 'option' => 'image'],
            ['name' => 'Header', 'option' => 'text']
        ]);
    }
}

