<?php

use App\Pickpoint;
use Illuminate\Database\Seeder;

class PickpointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pickpoint::create([
            'name' => 'Тестовая точка выдачи',
            'address' => 'Тестовый адрес',
            'phone' => '+7 777 777 77 77',
            'worktime' => '10:00 - 21:00',
            'email' => 'secret@secret.com'
        ]);
    }
}
