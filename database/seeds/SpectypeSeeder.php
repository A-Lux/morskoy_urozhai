<?php

use App\Spectype;
use Illuminate\Database\Seeder;

class SpectypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Spectype::create([
            'name' => 'checkbox'
        ]);

        Spectype::create([
            'name' => 'integer_field'
        ]);
    }
}
