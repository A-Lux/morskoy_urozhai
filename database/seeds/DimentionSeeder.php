<?php

use App\Dimention;
use Illuminate\Database\Seeder;

class DimentionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dimention::create([
            'name' => 'грам',
            'slug' => 'gram',
            'short_name' => 'гм',
            'dimention_type_id' => 1
        ]);

        Dimention::create([
            'name' => 'килограм',
            'slug' => 'kilogram',
            'short_name' => 'кг',
            'dimention_type_id' => 1
        ]);

        Dimention::create([
            'name' => 'Дней',
            'slug' => 'days',
            'short_name' => 'д',
            'dimention_type_id' => 2
        ]);

        Dimention::create([
            'name' => 'Месяцев',
            'slug' => 'months',
            'short_name' => 'м',
            'dimention_type_id' => 2
        ]);
    }
}
