<?php

use App\Specification;
use Illuminate\Database\Seeder;

class SpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specification::create([
            'name' => 'Вес',
            'slug' => 'weight',
            'spectype_id' => 1,
            'dimention_type_id' => 1
        ]);

        Specification::create([
            'name' => 'Срок годности',
            'slug' => 'lifeline',
            'spectype_id' => 1,
            'dimention_type_id' => 2
        ]);
    }
}
