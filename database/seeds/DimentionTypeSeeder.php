<?php

use App\DimentionType;
use Illuminate\Database\Seeder;

class DimentionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DimentionType::create([
            'name' => 'Вес'
        ]);

        DimentionType::create([
            'name' => 'Время'
        ]);
    }
}
