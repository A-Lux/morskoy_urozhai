<?php

use App\DeliveryType;
use Illuminate\Database\Seeder;

class DeliveryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryType::create([
            'name' => 'Доставка',
            'is_active' => 1
        ]);

        DeliveryType::create([
            'name' => 'Самовывоз',
            'is_active' => 1
        ]);
    }
}
