<?php

use App\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'header' => 'About',
            'url' => '/about', 
            'title' => 'About', 
            'description' => 'About'
        ]);

        Page::create([
            'header' => 'Главная',
            'url' => '/', 
            'title' => 'Главная', 
            'description' => 'Главная'
        ]);
        
        Page::create([
            'header' => 'Доставка',
            'url' => '/delivery', 
            'title' => 'Доставка', 
            'description' => 'Доставка'
        ]);
        
        Page::create([
            'header' => 'Поставщикам',
            'url' => '/wholesalers', 
            'title' => 'Поставщикам', 
            'description' => 'Поставщикам'
        ]);

        Page::create([
            'header' => 'Поставщикам',
            'url' => '/wholesalers', 
            'title' => 'Поставщикам', 
            'description' => 'Поставщикам'
        ]);
    }
}
