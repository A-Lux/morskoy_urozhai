<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_information', function (Blueprint $table) {
            $table->id();
            $table->string('street');
            $table->string('house_building')->nullable();
            $table->string('porch')->nullable();
            $table->smallInteger('floor')->nullable();
            $table->string('house');
            $table->string('apartments')->nullable();
            $table->string('office')->nullable();
            $table->timestamp('deliver_at')->nullable();;
            $table->foreignId('order_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_informations');
    }
}
