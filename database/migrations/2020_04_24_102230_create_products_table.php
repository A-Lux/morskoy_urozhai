<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 50);
            $table->string('name', 100);
            $table->string('slug', 100)->nullable();
            $table->text('description');
            $table->string('thumbnail')->nullable();
            $table->text('slider_images')->nullable();
            $table->integer('price');
            $table->integer('discount')->nullable();
            $table->boolean('is_popular')->default(0);
            $table->boolean('is_offer')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
