<?php

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/locales/{locale}', 'LocaleController@setLocale')->name('set.locale');
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('products/{product:slug}', 'ProductController@show')->name('products.show');
Route::get('recipes', 'RecipeController@index')->name('recipe.index');
Route::get('recipes/{category:slug}', 'RecipeController@recipesByCategory')->name('recipe.by-category');
Route::get('recipes/{category:slug}/{recipe:slug}', 'RecipeController@show')->name('recipe.show');
Route::get('cart', 'CartController@index')->name('cart.index');
Route::get('catalog/{category:slug}', 'CatalogController@show')->name('catalog.show');
Route::get('promotions/{promotion:slug}', 'PromotionController@show')->name('promotions.show');
Route::get('search', 'SearchController@search')->name('search.index');
Route::get('password/reset/{token}', 'PasswordResetController@index')->name('password-reset.index');
Route::get('cabinet/{any?}', 'CabinetController@index')->where('any', '.*')->name('cabinet');

Route::prefix('orders')
    ->group(function () {
        Route::get('failure', 'OrderController@failure')->name('order.failure');
        Route::get('success', 'OrderController@success')->name('order.success');
    });

Route::get('contacts', 'ContactController@index')->name('contacts.index');

Route::get('about', 'AboutController@index');

Route::get('delivery', 'DeliveryController@index');
Route::get('wholesalers', 'WholesalersController@index');

Route::get('blogs', 'BlogController@index')->name('blog.index');
Route::get('blogs/{blog:slug}', 'BlogController@show')->name('blog.show');

Route::get('coupon', function () {
    return view('coupon');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/bills/{order}', 'Voyager\BillController@show')->name('admin.bill.show');
    Voyager::routes();
});

Route::prefix('pagebuilder')
    ->middleware(['auth'])
    ->group(function (){
        Route::get('types', 'PageBuilderController@types');
        Route::post('/', 'PageBuilderController@update');
    });