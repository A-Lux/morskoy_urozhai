<?php

use App\Product;
use App\Rules\CartProductsInfo;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    // Auth
    Route::post('login', 'LoginController@login');
    Route::post('register', 'RegisterController@register');

    Route::prefix('password')
        ->group(function () {
            Route::post('create', 'PasswordResetController@create');
            Route::get('find/{token}', 'PasswordResetController@find');
            Route::post('reset', 'PasswordResetController@reset');
        });

    Route::middleware('auth:api')
        ->group(function () {
            Route::post('logout', 'LogoutController@logout');
            Route::prefix('users')
                ->group(function () {
                    Route::get('/', 'UserController@user');
                    Route::put('/', 'UserController@update');
                    Route::get('active-address', 'UserController@getActiveAddress');
                    Route::get('orders', 'UserController@orders');
                    Route::get('orders/{order}', 'UserController@order');
                    Route::get('favourites', 'UserController@favourites');
                });
        });
    // END Auth

    // Specifications
    Route::get('specifications', 'SpecificationController@index');
    Route::get('specifications/{specification}', 'SpecificationController@specification');
    Route::get('specifications/{specification}/dimentions', 'SpecificationController@dimentions');
    Route::get('categories/{category}/filters', 'CatalogController@filters');
    Route::get('filters', 'FilterController@filter');
    Route::get('filters/promotions/{promotion}', 'FilterController@filterPromotions');
    Route::get('products/{product}/specifications', 'ProductController@specifications');
    Route::get('search-words/{word}', 'SearchController@searchWords');
    Route::get('promotions/{promotion}/filters', 'PromotionController@filters');
    // Cart
    Route::prefix('cart')
        ->group(function () {
            Route::get('/', 'CartController@getCart');
            Route::post('add', 'CartController@add')->middleware('in.stock');
            Route::post('add-group', 'CartController@addGroup')->middleware('in.stock');
            Route::post('remove', 'CartController@remove');
            Route::post('clear', 'CartController@clear');
        });

    Route::post('reviews/company', 'CompanyReviewController@send');
    // Authenticated group
    Route::middleware(['auth:api'])
        ->group(function () {
            // Reviews
            Route::prefix('reviews')
                ->group(function () {
                    Route::post('products/{product}', 'ReviewController@product');
                    Route::post('recipes/{recipe}', 'ReviewController@recipe');
                    Route::post('{review}/upvote', 'ReviewController@upvote');
                    Route::post('{review}/downvote', 'ReviewController@downvote');
                });

            // Favourites
            Route::prefix('favourites')
                ->group(function () {
                    Route::get('/', 'FavouriteController@get');
                    Route::post('add', 'FavouriteController@add');
                    Route::delete('remove', 'FavouriteController@remove');
                });

            Route::get('promocodes', 'PromocodeController@promocodes');
            Route::get('promocodes/{promocode}', 'PromocodeController@check')->name('promocodes.check');
        });

    Route::get('payment-types', 'PaymentTypeController@paymentTypes');
    Route::get('delivery-types', 'PaymentTypeController@deliveryTypes');
    Route::get('pickpoints', 'PaymentTypeController@pickpoints');

    Route::post('requests', 'RequestController@request');
    Route::prefix('orders')
        ->group(function () {
            Route::post('/', 'OrderController@store');
        });

    Route::get('recipes', 'RecipeController@recipes');
    Route::get('recipes/{category}', 'RecipeController@recipesByCategory');
    Route::get('recipes/{recipe}/reviews', 'RecipeController@reviews');
    Route::get('products/{product}/reviews', 'ProductController@reviews');

    Route::get('categories/{category}/children', 'CategoryController@children');
    Route::get('/cart/products/info', function (Request $request) {
        $request->validate([
            'products' => 'array',
            'products.*' => 'exists:products,id',
            'q' => 'array'
        ]);
        return Product::whereIn('id', $request->products)->get()->each(function ($product) use ($request) {
            $d = [];
            foreach ($request->q as $w) {
                $d[] = json_decode($w);
            }
            $temp = collect($d);
            $product->q = $temp->where('product_id', $product->id)->first()->amount;
        });
    });
});
