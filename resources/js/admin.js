import Vue from 'vue';
import Vuex from 'vuex';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(Vuex);
Vue.use(VueSweetalert2);

import Contact from './views/Admin/Contact';
import Recipe from './views/Admin/Recipe';
import Product from './views/Admin/Product';

window.axios = require('axios');

const app = new Vue({
    el: '#app',
    components: {
        Contact,
        Recipe,
        Product
    }
});