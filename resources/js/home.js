require("./bootstrap");
require("slick-carousel");
import NProgress from "nprogress";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

function addToCart(id) {
    return apiClient.post(`/api/cart/add?id=${id}&q=1`);
}

$(".categories.slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 7.999999,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    dots: false,
    arrows: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5.999999
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3.999999,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2.999999,
                arrows: false
            }
        }
    ]
});

$(".offers.slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    dots: false,
    prevArrow: $(".offer-prev"),
    nextArrow: $(".offer-next"),
    rows: 0,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                arrows: false
            }
        }
    ]
});
$(".product.slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    dots: false,
    arrows: true,
    rows: 0,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                arrows: false
            }
        }
    ]
});

const addToCartBtns = document.querySelectorAll(".cart-btn");
const goodsValue = document.getElementById("goods-value");

addToCartBtns.forEach(btn => {
    btn.addEventListener("click", e => {
        let data = [];
        data.push({
            quantity: {
                product_id: btn.getAttribute("data-product-id"),
                amount: 1
            },
            id: btn.getAttribute("data-product-id")
        });
        if (parseInt(btn.getAttribute("in-stock"))) {
            if (localStorage.getItem("products") == null) {
                localStorage.setItem("products", JSON.stringify(data));
                Toast.fire({
                    icon: "success",
                    title: "Товар добавлен в корзину"
                });
                if (localStorage.getItem("totalPrice") == null) {
                    localStorage.setItem(
                        "totalPrice",
                        parseInt(btn.getAttribute("product-price"))
                    );
                } else {
                    localStorage.setItem(
                        "totalPrice",
                        parseInt(btn.getAttribute("product-price")) +
                            parseInt(localStorage.getItem("totalPrice"))
                    );
                }
                goodsValue.innerHTML = `${localStorage.getItem(
                    "totalPrice"
                )} тг`;
            } else {
                let index = JSON.parse(
                    localStorage.getItem("products")
                ).findIndex(
                    object => object.id == btn.getAttribute("data-product-id")
                );
                if (index == -1) {
                    let basketStorage = JSON.parse(
                        localStorage.getItem("products")
                    );
                    basketStorage.push({
                        quantity: {
                            product_id: btn.getAttribute("data-product-id"),
                            amount: 1
                        },
                        id: btn.getAttribute("data-product-id")
                    });
                    localStorage.setItem(
                        "products",
                        JSON.stringify(basketStorage)
                    );
                    Toast.fire({
                        icon: "success",
                        title: "Товар добавлен в корзину"
                    });
                    goodsValue.innerHTML = `${localStorage.getItem(
                        "totalPrice"
                    )} тг`;
                    if (localStorage.getItem("totalPrice") == null) {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(btn.getAttribute("product-price"))
                        );
                    } else {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(btn.getAttribute("product-price")) +
                                parseInt(localStorage.getItem("totalPrice"))
                        );
                    }
                    goodsValue.innerHTML = `${localStorage.getItem(
                        "totalPrice"
                    )} тг`;
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "Товар добавлен"
                    });
                }
            }
        } else {
            Toast.fire({
                icon: "warning",
                title: "Товара нет в наличии"
            });
        }
    });
});
