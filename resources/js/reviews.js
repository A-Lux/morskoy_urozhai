import Vue from "vue";
import store from "./vuex/ReviewsStore.js";
import ReviewsComponent from "./components/ReviewsComponent.vue";

Vue.component("reviews-component", ReviewsComponent);

const app = new Vue({
    el: "#app",
    store
});
