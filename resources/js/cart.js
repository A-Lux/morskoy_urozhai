import Vue from "vue";
import store from "./vuex/CartStore.js";
import CartComponent from "./components/CartComponent.vue";

Vue.component("cart-component", CartComponent);

const app = new Vue({
    el: "#app",
    store
});
