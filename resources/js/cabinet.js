import Vue from "vue";
import store from "./vuex/CabinetStore.js";
import VueRouter from "vue-router";
import { CollapsePlugin } from "bootstrap-vue";
import Vuellidate from "vuelidate";

Vue.use(CollapsePlugin);
Vue.use(VueRouter);
Vue.use(Vuellidate);

import CabinetComponent from "./components/CabinetComponent.vue";
import Profile from "./views/Profile.vue";
import Favorites from "./views/Favorites.vue";
import Orders from "./views/Orders.vue";
import { AuthService } from "./services/AuthService.js";

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

Vue.component("cabinet-component", CabinetComponent);
const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "",
            name: "profile",
            component: Profile
        },
        {
            path: "/favourites",
            name: "favorites",
            component: Favorites
        },
        {
            path: "/orders",
            name: "orders",
            component: Orders
        }
        // { path: '*', component: NotFound }
    ]
});

router.beforeEach((to, from, next) => {
    apiClient
        .get("/api/users", config)
        .then(() => {
            next();
        })
        .catch(() => {
            window.location.replace(window.location.origin);
        });
});

const app = new Vue({
    el: "#app",
    store,
    router
});
