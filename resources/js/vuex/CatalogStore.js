import Vue from "vue";
import Vuex from "vuex";
import CatalogService from "../services/CatalogService.js";

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        total: null,
        currentPage: null,
        filters: [],
        products: [],
        maxprice: null,
        params: {}
    },
    mutations: {
        SET_FILTERS(state, filters) {
            state.filters = filters;
        },
        SET_PARAMS(state, index) {
            state.params[index] = [];
        },
        SET_PRODUCTS(state, products) {
            state.products = products;
        },
        SET_MAXPRICE(state, price) {
            state.maxprice = price;
        },
        SET_TOTAL(state, total) {
            state.total = total;
        },
        SET_CURRENTPAGE(state, currentPage) {
            state.currentPage = currentPage;
        },
        PUSH_PRODUCTS(state, products) {
            state.products.push(products);
        }
    },
    actions: {
        clearFilters({ commit }) {
            for (const key in this.state.filters) {
                const element = this.state.filters[key];
                if (element[0].slug) {
                    commit("SET_PARAMS", element[0].slug);
                } else if (key == "Категории") {
                    commit("SET_PARAMS", "category");
                }
            }
        },
        fetchFilters({ commit }, { category, type }) {
            CatalogService.getFilters(category, type)
                .then(response => {
                    commit("SET_FILTERS", response.data.filters);
                    commit("SET_MAXPRICE", response.data.maxPrice);
                    for (const key in response.data.filters) {
                        const element = response.data.filters[key];
                        if (element[0] && element[0].slug) {
                            commit("SET_PARAMS", element[0].slug);
                        } else if (key == "Категории") {
                            commit("SET_PARAMS", "category");
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchProducts({ commit }, { parametrs, type }) {
            CatalogService.getProducts(parametrs, type)
                .then(response => {
                    commit("SET_PRODUCTS", response.data.data);
                    commit("SET_TOTAL", response.data.total);
                    commit("SET_CURRENTPAGE", response.data.current_page);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchMoreProducts({ commit }, { parametrs, type }) {
            CatalogService.getProducts(parametrs, type)
                .then(response => {
                    for (const key in response.data.data) {
                        commit("PUSH_PRODUCTS", response.data.data[key]);
                    }
                    commit("SET_CURRENTPAGE", response.data.current_page);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    },
    getters: {}
});
