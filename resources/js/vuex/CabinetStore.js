import Vue from "vue";
import Vuex from "vuex";
import CabinetService from "../services/CabinetService.js";
import Swal from "sweetalert2";
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        userData: [],
        favorites: [],
        orders: [],
        orderProducts: {}
    },
    mutations: {
        SET_DATA(state, data) {
            state.userData = data;
        },
        SET_FAVORITES(state, favorites) {
            state.favorites = favorites;
        },
        REMOVE_FAVORITE(state, id) {
            let index = state.favorites.findIndex(
                favorite => favorite.id == id
            );
            state.favorites.splice(index, 1);
        },
        SET_ORDERS(state, orders) {
            state.orders = orders;
        },
        SET_ORDER_PRODUCTS(state, { products, id }) {
            state.orderProducts[id] = products;
            state.orderProducts = { ...state.orderProducts };
        }
    },
    actions: {
        fetchData({ commit }) {
            CabinetService.getData()
                .then(response => {
                    commit("SET_DATA", response.data);
                    this.state.userData.birthday = new Date(
                        response.data.birthday
                    );
                    for (const key in response.data.addresses) {
                        for (const k in response.data.addresses[key]) {
                            if (response.data.addresses[key][k] == "null") {
                                this.state.userData.addresses[key][k] = "";
                            }
                        }
                    }
                    if (!response.data.addresses.length) {
                        this.state.userData.addresses = [
                            {
                                street: "",
                                porch: "",
                                house_building: "",
                                house: "",
                                office: "",
                                apartments: "",
                                floor: ""
                            }
                        ];
                    }
                })
                .catch(error => {});
        },
        fetchFavorites({ commit }) {
            CabinetService.getFavorites()
                .then(response => {
                    commit("SET_FAVORITES", response.data);
                })
                .catch(error => {});
        },
        removeFavorite({ commit }, id) {
            CabinetService.removeFavorite(id).then(response => {
                commit("REMOVE_FAVORITE", id);
                Toast.fire({
                    icon: "success",
                    title: response.data.message
                });
            });
        },
        fetchOrders({ commit, state }) {
            CabinetService.getOrders().then(response => {
                commit("SET_ORDERS", response.data);
                for (const key in response.data) {
                    state.orderProducts[response.data[key].id] = [];
                }
            });
        },
        fetchOrderProducts({ commit, state }, order) {
            const orderKeys = Object.keys(state.orderProducts);
            const orderIdx = orderKeys.indexOf(order);
            if (state.orderProducts[orderKeys[orderIdx]].length == 0) {
                CabinetService.getOrderProducts(order).then(response => {
                    commit("SET_ORDER_PRODUCTS", {
                        products: response.data,
                        id: order
                    });
                });
            }
        }
    },
    getters: {}
});
