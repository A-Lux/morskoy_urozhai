import Vue from "vue";
import Vuex from "vuex";
import CartService from "../services/CartService.js";
import { pick } from "lodash";
const goodsValue = document.getElementById("goods-value");
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        products: [],
        payments: [],
        delivery: [],
        total: null,
        quantity: null,
        promocodeError: null,
        address: {},
        pickpoints: [],
        userData: {},
        is_empty: false
    },
    mutations: {
        SET_PRODUCTS(state, products) {
            state.products = products;
        },
        SET_PAYMENTS(state, payments) {
            state.payments = payments;
        },
        SET_DELIVERY(state, delivery) {
            state.delivery = delivery;
        },
        SET_TOTALPRICE(state) {
            let sum = 0;
            for (const index in state.products) {
                sum += state.products[index].price * state.products[index].q;
            }
            goodsValue.innerHTML = `${sum} тг`;
            localStorage.setItem("totalPrice", sum);
            state.total = sum;
        },
        SET_QUANTITY(state, quantity) {
            state.quantity = quantity;
        },
        SET_ACTIVE_ADDRESS(state, address) {
            state.address = address;
        },
        SET_PICKPOINTS(state, pickpoints) {
            state.pickpoints = pickpoints;
        },
        REMOVE_PRODUCT(state, id) {
            let basketStorage = JSON.parse(localStorage.getItem("products"));
            let index = state.products.findIndex(product => product.id == id);
            if (index > -1) {
                state.products.splice(index, 1);
                basketStorage.splice(index, 1);
            }
            localStorage.setItem("products", JSON.stringify(basketStorage));
            let sum = 0;
            for (const index in state.products) {
                sum += state.products[index].price * state.products[index].q;
            }
            goodsValue.innerHTML = `${sum} тг`;
            localStorage.setItem("totalPrice", sum);
            state.total = sum;
        },
        CLEAR_PRODUCT(state) {
            localStorage.removeItem("products");
            localStorage.setItem("totalPrice", 0);
            state.products = [];
        },
        SET_USERDATA(state, data) {
            state.userData = data;
        },
        SET_EMPTY(state, empty) {
            state.is_empty = empty;
        }
    },
    actions: {
        fetchUserData({ commit }) {
            CartService.getData().then(response => {
                commit("SET_USERDATA", response.data);
            });
        },
        fetchProducts({ commit }) {
            let products = JSON.parse(localStorage.getItem("products"));
            if (products && products.length) {
                let parametrs = {
                    products: [],
                    q: []
                };
                for (const product of products) {
                    parametrs["products"].push(product.id);
                    parametrs["q"].push(product.quantity);
                }
                CartService.getProducts(parametrs)
                    .then(response => {
                        commit("SET_PRODUCTS", response.data);
                        if (response.data.length) {
                            commit("SET_EMPTY", false);
                        } else {
                            commit("SET_EMPTY", true);
                        }
                        commit("SET_TOTALPRICE");
                        commit("SET_QUANTITY", response.data.q);
                    })
                    .catch(error => {});
            } else {
                commit("SET_EMPTY", true);
            }
        },
        fetchAddress({ commit }) {
            CartService.getActiveAddress()
                .then(response => {
                    commit("SET_ACTIVE_ADDRESS", response.data);
                })
                .catch(error => console.log(error));
        },
        fetchPickpoints({ commit }) {
            CartService.getPickPoints()
                .then(response => {
                    commit("SET_PICKPOINTS", response.data);
                })
                .catch(error => console.log(error));
        },
        removeProduct({ commit }, id) {
            commit("REMOVE_PRODUCT", id);
        },
        clearCart({ commit }) {
            CartService.clearCart().then(response => {
                commit("SET_EMPTY", true);
                commit("CLEAR_PRODUCT");
                goodsValue.innerHTML = `${response.data.total.toLocaleString()} тг`;
            });
        },
        fetchPayments({ commit }) {
            CartService.getPayments()
                .then(response => {
                    commit("SET_PAYMENTS", response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchDelivery({ commit }) {
            CartService.getDelivery()
                .then(response => {
                    commit("SET_DELIVERY", response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        addCount({ commit }, { id, quantity }) {
            commit("SET_TOTALPRICE");
            let products = JSON.parse(localStorage.getItem("products"));
            products.forEach(item => {
                if (item.id == id) {
                    item.quantity.amount = quantity
                } else {
                    return false
                }
            let productAdd = JSON.stringify(products)
            localStorage.setItem('products', productAdd)
            });
        }
    },
    getters: {}
});
