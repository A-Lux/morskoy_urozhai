import Vue from "vue";
import Vuex from "vuex";
import ReviewsService from "../services/ReviewsService.js";
import NProgress from "nprogress";
import Swal from "sweetalert2";
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        total: null,
        currentPage: null,
        reviews: []
    },
    mutations: {
        SET_REVIEWS(state, reviews) {
            state.reviews = reviews;
        },
        SET_TOTAL(state, total) {
            state.total = total;
        },
        SET_CURRENTPAGE(state, currentPage) {
            state.currentPage = currentPage;
        },
        PUSH_REVIEWS(state, reviews) {
            state.reviews.push(reviews);
        },
        UNSHIFT_REVIEW(state, review) {
            state.reviews.unshift(review);
        },
        SET_UPVOTE(state, id) {
            let index = state.reviews.findIndex(review => review.id == id);
            state.reviews[index].downvotes_count -= 1;
            state.reviews[index].upvotes_count += 1;
        },
        SET_DOWNVOTE(state, id) {
            let index = state.reviews.findIndex(review => review.id == id);
            state.reviews[index].upvotes_count -= 1;
            state.reviews[index].downvotes_count += 1;
        }
    },
    actions: {
        fetchReviews({ commit }, { id, type }) {
            ReviewsService.getReviews(id, type)
                .then(response => {
                    commit("SET_REVIEWS", response.data.data);
                    commit("SET_TOTAL", response.data.total);
                    commit("SET_CURRENTPAGE", response.data.current_page);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchMoreReviews({ commit }, { id, type, page }) {
            ReviewsService.getReviews(id, type, page)
                .then(response => {
                    for (const key in response.data.data) {
                        commit("PUSH_REVIEWS", response.data.data[key]);
                    }
                    commit("SET_CURRENTPAGE", response.data.current_page);
                    commit("SET_TOTAL", response.data.total);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        postReview({ commit }, { id, type, review }) {
            ReviewsService.postReview(id, type, review)
                .then(response => {
                    commit("UNSHIFT_REVIEW", response.data.review);
                    document.querySelector("#nav-reviews-tab span").innerText =
                        response.data.reviewsCount;
                })
                .catch(error => {
                    if (error.response.status == 401) {
                        Toast.fire({
                            icon: "error",
                            title: error.response.data.message
                        });
                    }
                    NProgress.done();
                });
        },
        upVote({ commit }, { id }) {
            ReviewsService.upVote(id)
                .then(response => {
                    if (response.status === 201) {
                        commit("SET_UPVOTE", id);
                    }
                })
                .catch(error => {
                    if (error.response.status == 401) {
                        Toast.fire({
                            icon: "error",
                            title: error.response.data.message
                        });
                    }
                    NProgress.done();
                });
        },
        downVote({ commit }, { id }) {
            ReviewsService.downVote(id)
                .then(response => {
                    if (response.status === 201) {
                        commit("SET_DOWNVOTE", id);
                    }
                })
                .catch(error => {
                    if (error.response.status == 401) {
                        Toast.fire({
                            icon: "error",
                            title: error.response.data.message
                        });
                    }
                    NProgress.done();
                });
        }
    },
    getters: {}
});
