import Vue from "vue";
import Vuex from "vuex";
import RecipesService from "../services/RecipesService.js";
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        total: null,
        currentPage: null,
        recipes: []
    },
    mutations: {
        SET_RECIPES(state, recipes) {
            state.recipes = recipes;
        },
        SET_TOTAL(state, total) {
            state.total = total;
        },
        SET_CURRENTPAGE(state, currentPage) {
            state.currentPage = currentPage;
        },
        PUSH_RECIPES(state, recipes) {
            state.recipes.push(recipes);
        }
    },
    actions: {
        fetchRecipes({ commit }, { category, page }) {
            RecipesService.getRecipes(category, page)
                .then(response => {
                    commit("SET_RECIPES", response.data.data);
                    commit("SET_TOTAL", response.data.total);
                    commit("SET_CURRENTPAGE", response.data.current_page);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchMoreRecipes({ commit }, { category, page }) {
            RecipesService.getRecipes(category, page)
                .then(response => {
                    for (const key in response.data.data) {
                        commit("PUSH_RECIPES", response.data.data[key]);
                    }
                    commit("SET_CURRENTPAGE", response.data.current_page);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    },
    getters: {}
});
