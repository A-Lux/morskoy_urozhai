import Swal from "sweetalert2";
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const passwordResetForm = document.querySelector('#password_reset_form');

passwordResetForm.addEventListener('submit', e => {
    e.preventDefault();
    const data = new FormData(passwordResetForm);
    axios.post('/api/password/reset', data)
        .then(response => {
            Toast.fire({
                icon: "success",
                title: response.data.message
            });
            document.querySelector('#reset_container').innerHTML += `<div class="py-4">
            <a href="#" data-toggle="modal" data-target="#authorizationModal">Войти</a></div>`;
        })
        .catch(errors => {
            const errs = document.querySelectorAll('.error');
            for (const err of errs) {
                err.innerHTML = '';
            }
            switch(errors.response.status) {
                case 422:
                    const validationErrors = errors.response.data.errors;
                    for (const key in validationErrors) {
                        const error = validationErrors[key];
                        document.querySelector(`.error-${key}`).innerHTML = error;
                    }
                    break;
                case 404:
                    Toast.fire({
                        icon: "error",
                        title: errors.response.data.message
                    });
                    break;
                default:
                    Toast.fire({
                        icon: "error",
                        title: 'Ошибка сервера, попробуйте позднее!'
                    });
            }
        });
});