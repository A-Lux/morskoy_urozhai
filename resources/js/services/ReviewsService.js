import axios from "axios";
import NProgress from "nprogress";

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});
const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

export default {
    getReviews(id, type, page) {
        if (type == "product") {
            return apiClient.get(
                `/api/products/${id}/reviews?page=${page ? page : 1}`
            );
        } else {
            return apiClient.get(
                `api/recipes/${id}/reviews?page=${page ? page : 1}`
            );
        }
    },
    postReview(id, type, review) {
        if (type == "product") {
            return apiClient.post(
                `/api/reviews/products/${id}`,
                review,
                config
            );
        } else {
            return apiClient.post(`api/reviews/recipes/${id}`, review, config);
        }
    },
    upVote(id) {
        return apiClient.post(`/api/reviews/${id}/upvote`, {}, config);
    },
    downVote(id) {
        return apiClient.post(`/api/reviews/${id}/downvote`, {}, config);
    }
};
