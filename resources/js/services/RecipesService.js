import axios from "axios";
import NProgress from "nprogress";
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

export default {
    getRecipes(category, page) {
        return apiClient.get(
            `/api/recipes/${category ? category : ""}?page=${page ? page : 1}`
        );
    }
};
