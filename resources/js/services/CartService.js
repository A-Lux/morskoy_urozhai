import axios from "axios";
import NProgress from "nprogress";
var qs = require("qs");
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(
    response => {
        NProgress.done();
        return response;
    },
    function(error) {
        NProgress.done();
        return Promise.reject(error);
    }
);

export default {
    getProducts(parametrs) {
        return apiClient.get(`/api/cart/products/info`, {
            params: parametrs
        });
    },
    removeProduct(id) {
        return apiClient.post(`/api/cart/remove?id=${id}`);
    },
    clearCart() {
        return apiClient.post("/api/cart/clear");
    },
    getPayments() {
        return apiClient.get("/api/payment-types");
    },
    getDelivery() {
        return apiClient.get("/api/delivery-types");
    },
    getData() {
        return apiClient.get(`/api/users`, config);
    },
    addCount(id, quantity) {
        return apiClient.post(`/api/cart/add?id=${id}&q=${quantity}`);
    },
    checkout(parametrs) {
        return apiClient.post(`/api/orders`, parametrs, config);
    },
    checkPromocode(promocode) {
        return apiClient.get(`/api/promocodes/${promocode}`, config);
    },
    getActiveAddress() {
        return apiClient.get(`/api/users/active-address`, config);
    },
    getPickPoints() {
        return apiClient.get("/api/pickpoints");
    }
};
