import axios from "axios";
import NProgress from "nprogress";
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});
const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};
apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

export default {
    putData(userData) {
        return apiClient.post(`/api/users`, userData, config);
    },
    getData() {
        return apiClient.get(`/api/users`, config);
    },
    getFavorites() {
        return apiClient.get(`/api/favourites`, config);
    },
    removeFavorite(id) {
        return apiClient.delete(`/api/favourites/remove?id=${id}`, config);
    },
    addToCart(id) {
        return apiClient.post(`/api/cart/add?id=${id}&q=1`);
    },
    getOrders() {
        return apiClient.get(`/api/users/orders`, config);
    },
    getOrderProducts(order) {
        return apiClient.get(`/api/users/orders/${order}`, config);
    },
    logOut() {
        return apiClient.post(`/api/logout`, {}, config);
    }
};
