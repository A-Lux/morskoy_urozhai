import axios from "axios";
import NProgress from "nprogress";
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

export default {
    getFilters(category, type) {
        return apiClient.get(`/api/${type}/${category}/filters`);
    },
    getProducts(parametrs, type) {
        if (type.type == "promotions") {
            return apiClient.get(`/api/filters/${type.type}/${type.entityId}`, {
                params: parametrs
            });
        } else {
            return apiClient.get(`/api/filters`, {
                params: parametrs
            });
        }
    },
    addToCart(id) {
        return apiClient.post(`/api/cart/add?id=${id}&q=1`);
    }
};
