import Swal from "sweetalert2";
window.axios = require("axios");

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

class Pagebuilder {
    constructor(cfg) {
        this.cfg = cfg;
        this.cfg.textEditorsCount = 0;
        if(this.cfg.el) {
            this.init();
        } else {
            console.error('Pagebuilder: el is required');
        }
    }

    async init() {
        this.blocks = document.querySelectorAll(this.cfg.el);
        await this.getTypes();
        for (const block of this.blocks) {
            let type;
            let target;
            let hash;
            if(block.hasAttribute('data-type')) {
                target = block;
            } else {
                target = block.firstElementChild;
            }

            type = target.getAttribute('data-type');
            hash = target.getAttribute('data-hash');
            const input = this.createInput(block, type);
            input.addEventListener('click' , e => e.stopPropagation());
            const saveBtn = this.createSaveBtn(block, input, hash, type);
            this.transformBlock(target, type, input);
            this.append(block, input, type);
            
            if(input.classList.contains('input-textarea')) {
                this.textEditorInit(input);
            } 
            else {
                this.append(block, saveBtn, type);
            }
        }
    }

    getTypes() {
        return axios.get('/pagebuilder/types')
            .then(response => this.types = response.data);
    }

    createInput(block, type) {
        if(this.types.indexOf(type) != -1) {
            return this[`create${type}`](block, type);
        }
        return null;
    }

    createImg(block) {
        let input;
        input = document.createElement('input');
        input.type = 'file';
        input.classList = 'pagebuilder-input img';
        input.addEventListener('change', e => {
            const selectedFile = e.target.files[0]
            const reader = new FileReader();
            reader.onload = function(event) {
                block.firstElementChild.src = event.target.result;
            };
            reader.readAsDataURL(selectedFile);
        });

        return input;
    }

    createTxt(block) {
        let input;
        input = document.createElement('textarea');
        input.classList = 'pagebuilder-input input-textarea';
        input.id = `input-tinymce${new Date().valueOf()}`;
        input.style.height = block.offsetHeight + 'px';
        input.style.width = block.offsetWidth + 'px';

        return input;
    }

    createBgImg(block) {
        let input;
        input = document.createElement('input');
        input.type = 'file';
        input.classList = 'pagebuilder-input img';
        input.addEventListener('change', e => {
            const selectedFile = e.target.files[0]
            const reader = new FileReader();
            reader.onload = function(event) {
                block.style.backgroundImage = `url(${event.target.result})`;
            };
            reader.readAsDataURL(selectedFile);
        });

        return input;
    }

    createHeader(block) {
        let input;
        input = document.createElement('input');
        input.type = 'text';
        input.classList = 'pagebuilder-input input';
        input.addEventListener('keydown', e => {
            e.target.style.width = ((e.target.value.length + 1) * 8) + 'px';
        });

        return input;
    }

    append(target, toAppend, type) {
        if(this.types.indexOf(type) != -1) {
            return this[`append${type}`](target, toAppend);
        }
    }

    getContent(input) {
        if(input.tagName == 'textarea') {
            return input.innerHTML;
        } else if(input.type == 'file') {
            return input.files[0];
        } else {
            return input.value;
        }
    }
    

    appendImg(target, toAppend) {
        target.appendChild(toAppend);
    }

    appendBgImg(target, toAppend) {
        this.appendImg(target, toAppend);
    }

    appendTxt(target, toAppend) {
        target.firstElementChild.appendChild(toAppend);
    }

    appendHeader(target, toAppend) {
        this.appendTxt(target, toAppend);
    }

    async textEditorInit(input) {
        await tinymce.init({
            selector: `#${input.id}`,
            plugins : "save",
            menubar : false,
            branding: false,
            elementpath: false,
            toolbar: 'save | styleselect | bold italic | alignleft aligncenter alignright alignjustify',
            statusbar: false,
            themes: "inlite",
            save_onsavecallback: this.textEditorSave,
            setup: function(editor) {
                editor.on('init', e => {
                    editor.getBody().style.backgroundColor = "transparent";
                    editor.iframeElement.style.height = editor.iframeElement.contentWindow.document.body.offsetHeight + 30 + 'px';
                    editor.iframeElement.offsetParent.querySelector('.mce-toolbar').style.display = 'none';
                });
                editor.addSidebar('toolbarToogle', {
                    tooltip: 'Toolbar toogle',
                    icon: 'settings',
                    onrender: function (api) {
                        document.querySelector('.mce-sidebar-panel').remove();
                    },
                    onshow: function (api) {
                        editor.iframeElement.offsetParent.querySelector('.mce-toolbar').style.display = 'block';
                    },
                    onhide: function (api) {
                        editor.iframeElement.offsetParent.querySelector('.mce-toolbar').style.display = 'none';
                    }
                });
            }
        });
    }

    textEditorSave(editor) {
        const data = new FormData();
        data.append('slug', editor.targetElm.parentNode.getAttribute('data-hash'));
        data.append('content', editor.getContent());
        data.append('type', editor.targetElm.parentNode.getAttribute('data-type'));
        axios.post('/pagebuilder', data)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: "Блок изменен!"
                });
            });
    }

    createSaveBtn(block, input, hash, type) {
        const btn = document.createElement('button');
        btn.innerText = 'Сохранить';
        btn.classList.add('pageblock-btn');
        if(type == 'BgImg' || type == 'Img') {
            btn.classList.add('bg-img');
        }
        btn.addEventListener('click', e => {
            e.stopPropagation();
            const data = new FormData();
            data.append('slug', hash);
            data.append('content', this.getContent(input));
            data.append('type', type);
            axios.post('/pagebuilder', data)
                .then(response => {
                    Toast.fire({
                        icon: "success",
                        title: "Блок изменен!"
                    });
                });
        });
        return btn;
    }

    transformBlock(block, type, input) {
        if(this.types.indexOf(type) != -1) {
            return this[`transform${type}`](block, input);
        }
    }

    transformTxt(block, input) {
        input.innerHTML = block.innerHTML;
        block.innerHTML = '';
    }

    transformImg(block, input) {
        block.classList.add('pageblock-img');
        input.classList.add('pageblock-input-img');
    }

    transformBgImg(block, input) {
        this.transformImg(block, input);
    }

    transformHeader(block, input) {
        input.value = block.innerHTML;
        input.style.width = ((block.innerHTML.length) * 4) + 'px';
        block.innerHTML = '';
    }
}

new Pagebuilder({
    el: '.pageblock'
});