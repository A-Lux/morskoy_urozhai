require("./bootstrap");
require("slick-carousel");
import NProgress from "nprogress";
import Swal from "sweetalert2";

const goodsValue = document.getElementById("goods-value");
const quantityControls = document.getElementById("product_quantity_controls");
const decreaseBtn = quantityControls.firstElementChild;
const increaseBtn = quantityControls.lastElementChild;
const quantityField = quantityControls.querySelector("input");
const isWeighted = parseInt(quantityControls.getAttribute("data-weighted"));

decreaseBtn.addEventListener("click", e => {
    let step = 1;
    if (isWeighted) {
        step = 0.1;
        if (quantityField.value > step) {
            quantityField.value = (quantityField.value - step).toFixed(1);
        }
    } else {
        if (quantityField.value > step) {
            quantityField.value = quantityField.value - step;
        }
    }
});
increaseBtn.addEventListener("click", e => {
    let step = 1;
    if (isWeighted) {
        step = 0.1;
        if (quantityField.value < 20) {
            quantityField.value = (
                parseFloat(quantityField.value) + step
            ).toFixed(1);
        }
    } else {
        quantityField.value = parseInt(quantityField.value) + step;
    }
});

quantityField.addEventListener("input", e => {
    let step = 1;
    if (isWeighted) {
        step = 0.1;
    }
    const value = parseFloat(btn.value);
    if (isNaN(value) || value < step) {
        btn.value = step;
    }
});

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

const cartBtn = document.querySelectorAll(".cart-btn");
const favoritesBtn = document.querySelector(".favorites-btn");

cartBtn.forEach(btn =>
    btn.addEventListener("click", function() {
        if (!btn.hasAttribute("data-product-id")) {
            let data = [];
            data.push({
                quantity: {
                    product_id: btn.getAttribute("product-id"),
                    amount: quantityField.value
                },
                id: btn.getAttribute("product-id")
            });
            if (parseInt(btn.getAttribute("in-stock"))) {
                if (localStorage.getItem("products") == null) {
                    localStorage.setItem("products", JSON.stringify(data));
                    Toast.fire({
                        icon: "success",
                        title: "Товар добавлен в корзину"
                    });

                    if (localStorage.getItem("totalPrice") == null) {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(
                                btn.getAttribute("product-price") *
                                    quantityField.value
                            )
                        );
                    } else {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(
                                btn.getAttribute("product-price") *
                                    quantityField.value
                            ) + parseInt(localStorage.getItem("totalPrice"))
                        );
                    }
                    goodsValue.innerHTML = `${localStorage.getItem(
                        "totalPrice"
                    )} тг`;
                } else {
                    let index = JSON.parse(
                        localStorage.getItem("products")
                    ).findIndex(
                        object => object.id == btn.getAttribute("product-id")
                    );
                    let amountIndex = JSON.parse(
                        localStorage.getItem("products")
                    ).findIndex(
                        object =>
                            object.id == btn.getAttribute("product-id") &&
                            object.quantity.amount == quantityField.value
                    );
                    if (index == -1) {
                        let basketStorage = JSON.parse(
                            localStorage.getItem("products")
                        );
                        basketStorage.push({
                            quantity: {
                                product_id: btn.getAttribute("product-id"),
                                amount: quantityField.value
                            },
                            id: btn.getAttribute("product-id")
                        });
                        localStorage.setItem(
                            "products",
                            JSON.stringify(basketStorage)
                        );
                        Toast.fire({
                            icon: "success",
                            title: "Товар добавлен в корзину"
                        });
                        if (localStorage.getItem("totalPrice") == null) {
                            localStorage.setItem(
                                "totalPrice",
                                parseInt(
                                    btn.getAttribute("product-price") *
                                        quantityField.value
                                )
                            );
                        } else {
                            localStorage.setItem(
                                "totalPrice",
                                parseInt(
                                    btn.getAttribute("product-price") *
                                        quantityField.value
                                ) + parseInt(localStorage.getItem("totalPrice"))
                            );
                        }
                        goodsValue.innerHTML = `${localStorage.getItem(
                            "totalPrice"
                        )} тг`;
                    } else if (index > -1 && amountIndex == -1) {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(
                                localStorage.getItem("totalPrice") -
                                    parseInt(
                                        JSON.parse(
                                            localStorage.getItem("products")
                                        )[index].quantity.amount *
                                            btn.getAttribute("product-price")
                                    )
                            ) +
                                parseInt(
                                    btn.getAttribute("product-price") *
                                        quantityField.value
                                )
                        );
                        goodsValue.innerHTML = `${localStorage.getItem(
                            "totalPrice"
                        )} тг`;
                        let basketStorage = JSON.parse(
                            localStorage.getItem("products")
                        );
                        basketStorage[index] = {
                            quantity: {
                                product_id: btn.getAttribute("product-id"),
                                amount: quantityField.value
                            },
                            id: btn.getAttribute("product-id")
                        };
                        localStorage.setItem(
                            "products",
                            JSON.stringify(basketStorage)
                        );
                        Toast.fire({
                            icon: "success",
                            title: "Товар добавлен в корзину"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "Товар добавлен"
                        });
                    }
                }
            } else {
                Toast.fire({
                    icon: "warning",
                    title: "Товара нет в наличии"
                });
            }
        } else {
            let data = [];
            data.push({
                quantity: {
                    product_id: btn.getAttribute("data-product-id"),
                    amount: 1
                },
                id: btn.getAttribute("data-product-id")
            });
            if (parseInt(btn.getAttribute("in-stock"))) {
                if (localStorage.getItem("products") == null) {
                    localStorage.setItem("products", JSON.stringify(data));
                    Toast.fire({
                        icon: "success",
                        title: "Товар добавлен в корзину"
                    });
                    if (localStorage.getItem("totalPrice") == null) {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(
                                btn.getAttribute("product-price") *
                                    quantityField.value
                            )
                        );
                    } else {
                        localStorage.setItem(
                            "totalPrice",
                            parseInt(
                                btn.getAttribute("product-price") *
                                    quantityField.value
                            ) + parseInt(localStorage.getItem("totalPrice"))
                        );
                    }
                    goodsValue.innerHTML = `${localStorage.getItem(
                        "totalPrice"
                    )} тг`;
                } else {
                    let index = JSON.parse(
                        localStorage.getItem("products")
                    ).findIndex(
                        object =>
                            object.id == btn.getAttribute("data-product-id")
                    );
                    if (index == -1) {
                        let basketStorage = JSON.parse(
                            localStorage.getItem("products")
                        );
                        basketStorage.push({
                            quantity: {
                                product_id: btn.getAttribute("data-product-id"),
                                amount: 1
                            },
                            id: btn.getAttribute("data-product-id")
                        });
                        localStorage.setItem(
                            "products",
                            JSON.stringify(basketStorage)
                        );
                        Toast.fire({
                            icon: "success",
                            title: "Товар добавлен в корзину"
                        });

                        if (localStorage.getItem("totalPrice") == null) {
                            localStorage.setItem(
                                "totalPrice",
                                parseInt(
                                    btn.getAttribute("product-price") *
                                        quantityField.value
                                )
                            );
                        } else {
                            localStorage.setItem(
                                "totalPrice",
                                parseInt(
                                    btn.getAttribute("product-price") *
                                        quantityField.value
                                ) + parseInt(localStorage.getItem("totalPrice"))
                            );
                        }
                        goodsValue.innerHTML = `${localStorage.getItem(
                            "totalPrice"
                        )} тг`;
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "Товар добавлен"
                        });
                    }
                }
            } else {
                Toast.fire({
                    icon: "warning",
                    title: "Товара нет в наличии"
                });
            }
        }
    })
);

favoritesBtn.addEventListener("click", function() {
    apiClient
        .post(
            `/api/favourites/add`,
            { id: this.getAttribute("product-id") },
            config
        )
        .then(response => {
            Toast.fire({
                icon: "success",
                title: "Товар добавлен в избранное"
            });
        })
        .catch(error => {
            if (error.response.status === 401) {
                Toast.fire({
                    icon: "error",
                    title: error.response.data.message
                });
            }
            NProgress.done();
        });
});

$(".slider-for").slick({
    lazyLoad: "ondemand",
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider-nav",
    rows: 0,
    infinite: true
});
$(".slider-nav").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    arrows: false,
    focusOnSelect: true,
    rows: 0,
    infinite: true
});

$(".product.slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    dots: false,
    arrows: true,
    rows: 0,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                arrows: false
            }
        }
    ]
});
