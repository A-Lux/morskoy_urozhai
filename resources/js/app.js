require("./bootstrap");
require("./search.js");
require("hc-offcanvas-nav");
import NProgress from "nprogress";
import Swal from "sweetalert2";

var $main_nav = $(".main-nav");
var $toggle = $(".toggle");

var defaultOptions = {
    disableAt: 767,
    customToggle: $toggle,
    navTitle: "Навигация",
    levelTitles: true,
    levelTitleAsBack: true,
    pushContent: "body > *",
    insertClose: false,
    closeLevels: false
};

$main_nav.hcOffcanvasNav(defaultOptions);

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const goodsValue = document.getElementById("goods-value");

if (localStorage.getItem("totalPrice") == null) {
    goodsValue.innerHTML = `0 тг`;
} else {
    goodsValue.innerHTML = `${localStorage.getItem("totalPrice")} тг`;
}

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

const feedbackBtns = document.querySelectorAll(".feedback-btn");
feedbackBtns.forEach(btn =>
    btn.addEventListener("click", () => {
        Swal.fire({
            title: "Отзыв о компании",
            confirmButtonText: "Оставить отзыв",
            focusConfirm: false,
            html: `<form class="review-form">
                  <div class="form-group">
                    <label>Имя
                      <input class="form-control" name="name" type="text" />
                    </label>
                  </div>
                  <div class="form-group">
                    <label>E-mail
                      <input class="form-control" name="email" type="email" />
                    </label>
                  </div>
                  <div class="form-group">
                      <textarea class="form-control" name="review"></textarea>
                  </div>
                </form>`,
            preConfirm: () => {
                let reviewForm = document.querySelector(".review-form");
                let review = new FormData(reviewForm);
                apiClient
                    .post(`/api/reviews/company`, review)
                    .then(response => {
                        Toast.fire({
                            icon: "success",
                            title: response.data.message
                        });
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        });
    })
);

let categories = document.querySelectorAll("[category-id]");
const categoryChildrens = document.getElementById("children-categories");
const categoryName = document.getElementById("category-name");
categories.forEach(category =>
    category.addEventListener("click", function() {
        let id = category.getAttribute("category-id");
        apiClient.get(`/api/categories/${id}/children`).then(response => {
            if (response.data.length) {
                categoryName.innerText = category.outerText;
                categoryChildrens.innerHTML = "";
                for (const children of response.data) {
                    let listItem = document.createElement("li");
                    let listImg = document.createElement("img");
                    let listParagraph = document.createElement("p");
                    let listLink = document.createElement("a");
                    listImg.src = `/storage/${children.icon}`;
                    listParagraph.innerText = children.name;
                    listLink.href = `/catalog/${children.slug}`;
                    listLink.appendChild(listImg);
                    listLink.appendChild(listParagraph);
                    listItem.appendChild(listLink);
                    listItem.setAttribute(
                        "style",
                        `--bg-color: ${children.background_color}`
                    );
                    categoryChildrens.appendChild(listItem);
                }
            } else {
                window.location.replace(
                    `/catalog/${category.getAttribute("category-slug")}`
                );
            }
        });
    })
);

if (window.matchMedia("(max-width: 768px)").matches) {
    let header = document.querySelector(".header"),
        spaced = document.querySelector(".spaced");
    if (spaced) {
        spaced.style.paddingTop = header.offsetHeight + 50 + "px";
    }
    var lastScrollTop = 0;
    window.onscroll = function() {
        var st = window.scrollY;
        if (lastScrollTop > st) {
            header.classList.remove("header-unstick");
        } else if (lastScrollTop < st) {
            header.classList.add("header-unstick");
        }
        lastScrollTop = st;
    };
}
