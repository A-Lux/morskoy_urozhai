require("./bootstrap");
import NProgress from "nprogress";
import Swal from "sweetalert2";
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

const cabinetLink = document.getElementById("cabinet-link");

cabinetLink.addEventListener("click", e => {
    apiClient
        .get("/api/users", config)
        .then(() => {
            window.location.replace("/cabinet");
        })
        .catch(error => {
            if (error.response.status == 401) {
                $("#authorizationModal").modal("show");
            }
            NProgress.done();
        });
});

const registerForm = document.getElementById("registrationForm");
const authorizationForm = document.getElementById("authorizationForm");
const passwordResetForm = document.getElementById("passwordResetForm");

registerForm.addEventListener("submit", function(e) {
    e.preventDefault();
    let data = new FormData(this);
    apiClient
        .post("/api/register", data)
        .then(response => {
            for (const key of data.entries()) {
                document.querySelector(
                    `#registrationForm [error-msg="${key[0]}"]`
                ).innerText = "";
            }
            localStorage.setItem("token", response.data.access_token);
            window.location.replace(response.data.redirect_url);
        })
        .catch(error => {
            if (error.response.status === 422) {
                for (const key of data.entries()) {
                    document.querySelector(
                        `#registrationForm [error-msg="${key[0]}"]`
                    ).innerText = "";
                }
                for (const err in error.response.data.errors) {
                    if (data.has(err)) {
                        document.querySelector(
                            `#registrationForm [error-msg="${err}"]`
                        ).innerText = error.response.data.errors[err];
                    }
                }
            }
            NProgress.done();
        });
});

authorizationForm.addEventListener("submit", function(e) {
    e.preventDefault();
    let data = new FormData(this);
    apiClient
        .post("api/login", data)
        .then(response => {
            for (const key of data.entries()) {
                document.querySelector(
                    `#authorizationForm [error-msg="${key[0]}"]`
                ).innerText = "";
            }
            localStorage.setItem("token", response.data.access_token);
            window.location.replace(response.data.redirect_url);
        })
        .catch(error => {
            if (error.response.status === 401) {
                for (const key of data.entries()) {
                    document.querySelector(
                        `#authorizationForm [error-msg="${key[0]}"]`
                    ).innerText = "";
                }
                Toast.fire({
                    icon: "error",
                    title: error.response.data.message
                });
            } else if (error.response.status === 422) {
                for (const key of data.entries()) {
                    document.querySelector(
                        `#authorizationForm [error-msg="${key[0]}"]`
                    ).innerText = "";
                }
                for (const err in error.response.data.errors) {
                    if (data.has(err)) {
                        document.querySelector(
                            `#authorizationForm [error-msg="${err}"]`
                        ).innerText = error.response.data.errors[err];
                    }
                }
            }
            NProgress.done();
        });
});

passwordResetForm.addEventListener("submit", function(e) {
    e.preventDefault();
    let data = new FormData(this);
    apiClient
        .post("/api/password/create", data)
        .then(response => {
            for (const key of data.entries()) {
                document.querySelector(
                    `#passwordResetForm [error-msg="${key[0]}"]`
                ).innerText = "";
            }
            localStorage.setItem("token", response.data.access_token);
        })
        .catch(error => {
            if (
                error.response.status === 422 ||
                error.response.status === 404
            ) {
                for (const key of data.entries()) {
                    document.querySelector(
                        `#passwordResetForm [error-msg="${key[0]}"]`
                    ).innerText = "";
                }
                for (const err in error.response.data.errors) {
                    if (data.has(err)) {
                        document.querySelector(
                            `#passwordResetForm [error-msg="${err}"]`
                        ).innerText = error.response.data.errors[err];
                    }
                }
            }
            NProrgess.done();
        });
});
