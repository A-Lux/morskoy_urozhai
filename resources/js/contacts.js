require("./bootstrap");
import NProgress from "nprogress";
import Swal from "sweetalert2";
ymaps.ready(init);

function init() {
    var coordinates = JSON.parse(
        document.getElementById("map").getAttribute("data-coordinates")
    );
    var myMap = new ymaps.Map(
        "map",
        {
            center: [coordinates.lat, coordinates.lng],
            zoom: 9
        },
        {
            searchControlProvider: "yandex#search"
        }
    );
    var myPlacemark = new ymaps.Placemark([coordinates.lat, coordinates.lng]);
    myMap.geoObjects.add(myPlacemark);
}

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});
const agreement = document.getElementById("agreement");
const feedbackForm = document.getElementById("feedback-form");
const feedbackBtn = document.querySelector("#feedback-form button");
if (!agreement.checked) {
    feedbackBtn.setAttribute("disabled", "disabled");
}
agreement.addEventListener("click", function(e) {
    if (!e.target.checked) {
        feedbackBtn.setAttribute("disabled", "disabled");
    } else {
        feedbackBtn.removeAttribute("disabled");
    }
});
feedbackForm.addEventListener("submit", function(e) {
    e.preventDefault();
    let data = new FormData(this);
    apiClient
        .post(`/api/requests`, data)
        .then(response => {
            for (const key of data.entries()) {
                document.querySelector(
                    `#feedback-form [error-msg="${key[0]}"]`
                ).innerText = "";
            }
            Toast.fire({
                icon: "success",
                title: response.data.message
            });
        })
        .catch(error => {
            if (error.response.status === 422) {
                for (const key of data.entries()) {
                    document.querySelector(
                        `#feedback-form [error-msg="${key[0]}"]`
                    ).innerText = "";
                }
                for (const err in error.response.data.errors) {
                    if (data.has(err)) {
                        document.querySelector(
                            `#feedback-form [error-msg="${err}"]`
                        ).innerText = error.response.data.errors[err];
                    }
                }
            }
            NProgress.done();
        });
});
