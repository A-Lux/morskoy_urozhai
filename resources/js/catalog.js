import Vue from "vue";
import store from "./vuex/CatalogStore.js";
import CatalogComponent from "./components/CatalogComponent.vue";
import { CollapsePlugin } from "bootstrap-vue";

Vue.use(CollapsePlugin);

Vue.component("catalog-component", CatalogComponent);

const app = new Vue({
    el: "#app",
    store
});
