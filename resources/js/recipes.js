import Vue from "vue";
import VueMasonry from "vue-masonry-css";
import store from "./vuex/RecipesStore.js";
import RecipesComponent from "./components/RecipesComponent.vue";

Vue.use(VueMasonry);

Vue.component("recipes-component", RecipesComponent);

const app = new Vue({
    el: "#app",
    store
});
