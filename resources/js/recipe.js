require("./bootstrap");
import NProgress from "nprogress";
import Swal from "sweetalert2";

const cartBtn = document.querySelector(".cart-btn");
const goodsValue = document.querySelector("#goods-value");
const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.request.use(function(config) {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(function(response) {
    NProgress.done();
    return response;
});

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

cartBtn.addEventListener("click", function(e) {
    const id = JSON.parse(e.target.getAttribute("data-products"));
    apiClient.post(`/api/cart/add-group`, { id }).then(response => {
        if (response.status == 201) {
            Toast.fire({
                icon: "success",
                title: "Товары добавлены в корзину"
            });
            goodsValue.innerHTML = `${response.data.total} тг`;
        } else if (response.status == 200) {
            Toast.fire({
                icon: "warning",
                title: "Товаров нет в наличии"
            });
        }
    });
});
