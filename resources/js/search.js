const timeDebounce = 200;
const _onKeyupSearchInputDebounced = _.debounce(
    _onKeyupSearchInput,
    timeDebounce
);
const base = document.querySelector("base").href;
const searchResultContainer = document.querySelector(".search-result");
const searchInput = document.querySelector(".search-field");
const searchForm = searchInput.parentNode;

const searchClient = axios.create({
    baseUrl: window.location.origin
});

searchInput.addEventListener("keyup", _onKeyupSearchInputDebounced);
searchInput.addEventListener("click", () => {
    if (searchResultContainer.style.display != "block") {
        searchResultContainer.style.display = "block";
    }
});

function _onKeyupSearchInput(event) {
    const word = event.target.value;
    if (word.length > 0) {
        searchClient
            .get(`/api/search-words/${word}`)
            .then(response => {
                searchResultContainer.innerHTML = "";
                for (const word of response.data) {
                    searchResultContainer.appendChild(createListItem(word));
                }
            })
            .catch(error => {
                if (error.response.status == 404) {
                    searchResultContainer.innerHTML =
                        "<li>По данному запросу ничего не найдено!</li>";
                }
            });
    } else {
        searchResultContainer.innerHTML = "";
    }
}

function createListItem(word) {
    const listItem = document.createElement("li");
    const listLink = document.createElement("a");
    const listImg = document.createElement("img");
    listImg.src = `/storage/${word.thumbnail}`;
    listLink.href = `/products/${word.slug}`;
    listLink.appendChild(listImg);
    listLink.innerHTML += word.name;
    listItem.appendChild(listLink);

    return listItem;
}

window.addEventListener("click", e => {
    if (
        !e.target.classList.contains("search-result") &&
        !e.target.classList.contains("search-field") &&
        searchResultContainer.style.display == "block"
    ) {
        searchResultContainer.style.display = "none";
    }
});
