@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/catalog.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="catalog">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">{{ $promotion->name }}</li>
                        </ul>
                    </div>
                    <div class="col-12">
                    <h1>{{ $promotion->name }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <catalog-component :type="`promotions`" :category="{{ $promotion->id }}"></catalog-component>
        </section>
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ mix('js/catalog.js') }}"></script>
@endpush
