<div class="product slider popular">
    @foreach ($popularProducts as $offer)
    <div class="product-card">
        <div class="product-header">
            <a href="{{ route('products.show', $offer->slug) }}">
                @if ($offer->price < $offer->old_price)
                <span class="tag">{{ $offer->getDiscountPercentage($offer->old_price, $offer->price) }} %</span>
                @endif
                <img data-lazy="{{ asset('storage/' . $offer->thumbnail) }}">
            </a>
        </div>
        <div class="product-body p-3">
            <p>{{ $offer->name }}</p>
            <span class="vendore-code">Штрих код {{ $offer->sku }}</span>
            <div class="d-flex align-items-center justify-content-between mt-4">
                <div>
                    <p class="price">@if ($offer->price < $offer->old_price) {{ $offer->price }} @else {{ $offer->old_price }} @endif тг</p>
                    @if ($offer->price < $offer->old_price)
                    <p class="old-price">{{ $offer->old_price }} тг</p>
                    @endif
                </div>
                <button product-price="{{ $offer->price }}" in-stock="{{ $offer->in_stock }}" data-product-id="{{ $offer->id }}" type="button" class="{{ $offer->in_stock ? 'cart-btn' : 'cart-btn empty' }} "><img src="{{ asset('images/header/cart.png') }}"></button>
            </div>
        </div>
    </div>
    @endforeach
</div>

