@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('/css/contacts.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="contacts">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Контакты</li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <h1>Контакты</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="contacts">
                <div class="container">
                    <div class="contacts-inner">
                        <div class="contacts-left">
                            <div class="phone-info">
                                <div>
                                    @isset ($contacts->contact['phone'])
                                    @foreach ($contacts->contact['phone'] as $phone)
                                    <div class="phone">
                                        <a href="tel:{{ \Str::of($phone['value'])->replaceMatches('/\s/', '') }}">{{ $phone['value'] }}</a>
                                    </div>
                                    @endforeach
                                    @endisset
                                    @isset($contacts->contact['email'])
                                    @foreach ($contacts->contact['email'] as $email)
                                    <div class="email">
                                        <a href="mailto:{{ \Str::of($email['value'])->replaceMatches('/\s/', '') }}">{{ $email['value'] }}</a>
                                    </div>
                                    @endforeach
                                    @endisset
                                </div>
                                @isset($contacts->contact['whatsapp'])
                                <div class="social-icon">
                                    <a href="https://api.whatsapp.com/send?phone={{ $contacts->contact['whatsapp'] }}"><img src="{{ asset('/images/whatsapp.png') }}"></a>
                                </div>
                                @endisset
                            </div>
                            <div class="address-info">
                                <h1>Адрес:</h1>
                                @isset($contacts->contact['address'])
                                    @foreach ($contacts->contact['address'] as $address)
                                    <p>{{ $address['value'] }}</p>
                                @endforeach
                                @endisset
                            </div>
                            <form class="feedback" id="feedback-form">
                                <h1>Обратная связь</h1>
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Имя">
                                    <p class="text-danger" error-msg="name"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="email" placeholder="Email">
                                    <p class="text-danger" error-msg="email"></p>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" placeholder="Сообщение"></textarea>
                                    <p class="text-danger" error-msg="message"></p>
                                </div>
                                <button type="submit">Отправить</button>
                                <div class="contacts-check">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox" id="agreement">
                                        <p>Даю согласие на обработку <a href="">персональных данных</a></p>
                                    </label>
                                    <!-- <input id="box1" type="checkbox" />
                                    <label for="box1"></label> -->
                                </div>
                            </form>
                        </div>
                        <div class="contacts-right">
                            <div id="map" style="width: 840px; height: 940px" data-coordinates='@json(collect($contacts->coordinates)->collapse())'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
</div>
</div>
@endsection

@push('scripts')
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
<script src="{{ mix('js/contacts.js') }}" type="text/javascript"></script>
@endpush
