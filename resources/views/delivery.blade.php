@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('/css/delivery.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="delivery">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">@pageBlock(['block' => 'delivery-header', 'property' => 'content'])</li>
                        </ul>
                    </div>
                    <div class="col-12 pageblock">
                        <h1 data-type="@pageBlock(['block' => 'delivery-header', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'delivery-header', 'property' => 'slug'])">@pageBlock(['block' => 'delivery-header', 'property' => 'content'])</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="pageblock">
                    <img src="{{ asset('storage') }}/@pageBlock(['block' => 'delivery-image', 'property' => 'content'])" data-type="@pageBlock(['block' => 'delivery-image', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'delivery-image', 'property' => 'slug'])">
                </div>
                <div class="delivery-content pageblock">
                    <div data-type="@pageBlock(['block' => 'delivery-p1', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'delivery-p1', 'property' => 'slug'])">
                        @pageBlock(['block' => 'delivery-p1', 'property' => 'content'])
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
