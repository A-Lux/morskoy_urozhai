@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('/css/delivery.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="delivery">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">@pageBlock(['block' => 'wholesalers-header', 'property' => 'content'])</li>
                        </ul>
                    </div>
                    <div class="col-12 pageblock">
                        <h1 data-type="@pageBlock(['block' => 'wholesalers-header', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'wholesalers-header', 'property' => 'slug'])">@pageBlock(['block' => 'wholesalers-header', 'property' => 'content'])</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="pageblock">
                    <img src="{{ asset('storage') }}/@pageBlock(['block' => 'wholesalers-image', 'property' => 'content'])" data-type="@pageBlock(['block' => 'wholesalers-image', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'wholesalers-image', 'property' => 'slug'])">
                </div>
                <div class="delivery-content pageblock">
                    <div data-type="@pageBlock(['block' => 'wholesalers-p1', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'wholesalers-p1', 'property' => 'slug'])">
                        @pageBlock(['block' => 'wholesalers-p1', 'property' => 'content'])
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
