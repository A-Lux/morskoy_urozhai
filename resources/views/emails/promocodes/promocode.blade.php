<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Морской урожай</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap"
            rel="stylesheet"
        />
        <style>
            .tooltip {
                position: relative;
                display: inline-block;
            }

            .tooltip .tooltiptext {
                visibility: hidden;
                width: 140px;
                background-color: #555;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px;
                position: absolute;
                z-index: 1;
                bottom: 150%;
                left: 50%;
                margin-left: -75px;
                opacity: 0;
                transition: opacity 0.3s;
            }

            .tooltip .tooltiptext::after {
                content: "";
                position: absolute;
                top: 100%;
                left: 50%;
                margin-left: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: #555 transparent transparent transparent;
            }

            .tooltip:hover .tooltiptext {
                visibility: visible;
                opacity: 1;
            }
        </style>
    </head>
    <body style="margin: 0; padding: 0;">
        <table
            style="font-family: 'Montserrat', sans-serif;"
            align="center"
            cellpadding="0"
            cellspacing="0"
            width="600"
        >
            <tr>
                <td
                    align="center"
                    bgcolor="#fff"
                    style="padding: 40px 0 30px 0;"
                >
                <a href="{{ route('home.index') }}">
                        <img
                            src="{{ asset('/images/header/logo.png') }}"
                            alt="Creating Email Magic"
                            style="display: block; object-fit: none;"
                        />
                    </a>
                </td>
            </tr>
            <tr>
                <td
                    bgcolor="#007EFF"
                    style="border-top-left-radius: 15px; border-top-right-radius: 15px;"
                >
                    <table
                        style="padding: 20px;"
                        cellpadding="0"
                        cellspacing="0"
                        width="100%"
                    >
                        <tr>
                            <th
                                style="color: #fff; font-size: 25px; font-weight: 700"
                            >
                                Здравствуйте {{ $user->name }}!
                            </th>
                        </tr>
                        <tr>
                            <td
                                style="color: #fff; font-size: 19px; font-weight: 400;"
                            >
                                <p>
                                    Поздравляем, Вы получаете купон, который
                                    можете использовать для покупок в нашем
                                    Интернет-магазине!
                                </p>
                                <div
                                    style="display: flex; justify-content: center;"
                                >
                                    <div
                                        class="tooltip"
                                        style="max-width: 180px; width: 100%;"
                                    >
                                        <span
                                            id="coupon-code"
                                            style="color: #fff;; font-weight: 700; font-size: 14px;
                                            line-height: 1.2;
                                            border-radius: 10px;
                                            background-color: #ff960d;

                                            width: 100%;
                                            height: 41px;
                                            cursor: pointer;
                                            display: flex;
                                            align-items: center;
                                            border: none;
                                            outline: none;
                                            justify-content: center;"
                                        >
                                            <span
                                                class="tooltiptext"
                                                id="myTooltip"
                                                >Скопировать</span
                                            >{{ $promocode->promocode }}
                                        </span>
                                    </div>
                                </div>
                                <p>
                                    При оформлении зака просто введите этот код
                                    в соответсвующее поле в корзине с товаром.
                                </p>
                                @php
                                    $expires = new \Carbon\Carbon($promocode->expires_at);
                                    $expires = $expires->format('H:i:s d-m-Y');  
                                @endphp
                                <p>
                                    Поспешите! Этот купон действителен до
                                    <span
                                        style="color: rgb(255, 126, 0); font-weight: 700;"
                                    >{{ $expires }}</span
                                    >
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td
                    bgcolor="#00395d"
                    align="center"
                    style="padding: 20px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;"
                >
                    <p style="color: #fff; font-size: 19px;">
                        &copy; 2020 Морской Урожай
                    </p>
                </td>
            </tr>
        </table>
    </body>
    <script>
        const btn = document.getElementById("coupon-code");
        btn.onclick = function() {
            document.execCommand("copy");
        };
        btn.addEventListener("copy", function(event) {
            event.preventDefault();
            var tooltip = document.getElementById("myTooltip");
            if (event.clipboardData) {
                tooltip.innerHTML = "";
                event.clipboardData.setData(
                    "text/plain",
                    btn.textContent.replace(/\s/g, "")
                );
                tooltip.innerHTML = event.clipboardData
                    .getData("text")
                    .replace(/\s/g, "");
            }
        });
    </script>
</html>
