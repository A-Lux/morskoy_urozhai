<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#007eff">
    <title>@yield('title', \Page::getTitle())</title>
    <meta name="description" content="@yield('description', \Page::getDescription())">
    <base href="@yield('basePath', url('/'))">
    @stack('styles')
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @if (auth()->check() && auth()->user()->isAdmin())
    <link rel="stylesheet" href="{{ asset('css/pagebuilder.css') }}">
    @endif
</head>
<body>
    <div class="header">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-6">
                        <a class="logo" href="{{ route('home.index') }}">
                            <img src="{{ asset('/images/header/logo.png') }}">
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-9 col-6">
                        <a class="toggle" href="#">
                            <span></span>
                        </a>
                        <nav class="main-nav">
                            <ul>
                                <li class="catalog-li">
                                    <a href="#">Каталог</a>
                                    <ul>
                                        @foreach ($popupCategories as $category)
                                        <li>
                                            @if ($category->children->count() > 0)
                                            <a href="#">
                                                {{ $category->name }}
                                            </a>
                                            <ul>
                                                @foreach ($category->children as $childMenuItem)
                                                    <li>
                                                    <a href="{{ route('catalog.show', $childMenuItem ? $childMenuItem->slug : $category->slug) }}">{{ $childMenuItem->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            @else
                                            <a href="/catalog/{{ $category->slug }}">
                                                {{ $category->name }}
                                            </a>
                                            @endif
                                        </li>
                                        @endforeach
                                     </ul>
                                </li>
                                @if (menu('site', '_json'))
                                @foreach (menu('site', '_json') as $menuItem)
                                <li>
                                    <a href="{{ url($menuItem->url) }}">{{ $menuItem->title }}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <ol>
                            <li><a href="https://wa.me/{{ str_replace(' ', '', setting('site.whatsapp')) }}">{{ setting('site.whatsapp') }}</a></li>
                            <li><a href="tel:{{ str_replace(' ', '', setting('site.phone')) }}">{{ setting('site.phone') }}</a></li>
                        </ol>
                    </div>
                    <div class="offset-md-4 offset-lg-0"></div>
                    <div class="col-lg-2 col-md-4 d-flex">
                        <button class="feedback-btn" type="button">Оставить отзыв</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="stick-point background-dodgerblue my-md-4 my-lg-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="inventory">
                            <button class="navbar-toggler" type="button" data-toggle="modal" data-target="#catalogModal">
                                <i class="fal fa-bars"></i>
                            </button>
                            <h2>Каталог товаров</h2>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 d-flex">
                        <form action="{{ route('search.index') }}" method="get">
                        <button type="submit" class="search-btn"><img src="{{ asset('images/header/search.png') }}"></button>
                            <input type="search" class="search-field" name="q" placeholder="Найти урожай" autocomplete="off" value="{{ request()->q }}">
                            <ul class="search-result">
                            </ul>
                        </form>
                    </div>
                    <div class="col-lg-5 col-md-5 col-12">
                        <nav class="minor-nav">
                            <ul>
                                <li class="dropdown">
                                    <a href="javascript:void(0)">
                                        <img src="{{ asset('images/header/percentage.png') }}">
                                        <p>Акции</p>
                                    </a>
                                    <div class="dropdown-menu">
                                        @foreach ($promotions as $promotion)
                                        <a class="dropdown-item" href="{{ route('promotions.show', $promotion->slug) }}">{{ $promotion->name }}</a>
                                        @endforeach
                                    </div>
                                </li>
                                <li>
                                    <a href="/cabinet/favourites">
                                        <img src="{{ asset('images/header/heart.png') }}">
                                        <p>Избранное</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('cart.index') }}">
                                        <img src="{{ asset('images/header/cart.png') }}">
                                        <p id="goods-value">{{ $total }} тг</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" id="cabinet-link">
                                        <img src="{{ asset('images/header/account.png') }}">
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @yield('content')
    <div class="footer" class="mt-5">
        <section class="py-5 my-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-4 col-12">
                        <h2>О компании</h2>
                        <nav>
                            <ul>
                                @if (menu('company', '_json'))
                                @foreach (menu('company', '_json') as $menuItem)
                                <li>
                                    <a href="{{ url($menuItem->url) }}">{{ $menuItem->title }}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-12">
                        <h2>Покупателям</h2>
                        <nav>
                            <ul>
                                @if (menu('customers', '_json'))
                                @foreach (menu('customers', '_json') as $menuItem)
                                <li>
                                    <a href="{{ url($menuItem->url) }}">{{ $menuItem->title }}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-12">
                        <ol>
                            <li><a href="https://wa.me/{{ str_replace(' ', '', setting('site.whatsapp')) }}">{{ setting('site.whatsapp') }}</a></li>
                            <li><a href="tel:{{ str_replace(' ', '', setting('site.phone')) }}">{{ setting('site.phone') }}</a></li>
                        </ol>
                        <p>Круглосуточно вы можете оформить заказ по телефону или получить ответы на любые интересующие вас вопросы.</p>
                        <p>Присоединяйтесь к нам в соц. сетях:
                            @if (setting('site.facebook'))
                            <a href="{{ setting('site.facebook') }}">
                                <img src="{{ asset('images/footer/facebook.png')}}">
                            </a>
                            @endif
                            @if (setting('site.instagram'))
                            <a href="{{ setting('site.instagram') }}">
                                <img src="{{ asset('images/footer/instagram.png') }}">
                            </a>
                            @endif
                        </p>
                        <p>Разработано в <a href="https://www.a-lux.kz/">A-lux</a></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="background-prussianblue py-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-4 col-12 d-flex align-items-center">
                        <h1>Есть вопрос? Напишите нам письмо</h1>
                    </div>
                    <div class="offset-lg-1"></div>
                    <div class="col-lg-5 col-md-8 mt-4 mt-md-0 col-12 d-flex flex-wrap justify-content-center justify-content-sm-between">
                        <a class="contact-us" href="{{ route('contacts.index') }}">Написать нам</a>
                        <button type="button" class="feedback-btn">Оставить отзыв</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{-- Catalog Modal --}}
    <div class="modal fade" id="catalogModal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="catalogModalTitle">
        <div class="modal-dialog  modal-dialog-centered" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="catalogModalTitle">Каталог товаров</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <i class="fal fa-times"></i>
                     </button>
                 </div>
                 <div class="modal-body">
                     <ul>
                        @foreach ($popupCategories as $category)
                        <li style="--bg-color: {{ $category->background_color }}">
                        <a href="javascript:void(0)" category-slug="{{ $category->slug }}" category-id="{{ $category->id }}">
                                <img src="{{ asset('storage/' . $category->icon) }}">
                                <p>{{ $category->name }}</p>
                            </a>
                        </li>
                        @endforeach
                     </ul>
                     <h2 id="category-name"></h2>
                     <ul id="children-categories"></ul>
                 </div>
             </div>
        </div>
     </div>
     <div class="modal fade" id="passwordResetModal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="passwordResetTitle">
        <div class="modal-dialog  modal-dialog-centered" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="passwordResetTitle">Авторизация</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <i class="fal fa-times"></i>
                     </button>
                 </div>
                 <div class="modal-body">
                    <form id="passwordResetForm">
                        <div class="form-group">
                            <label>Введите почту
                                <input type="email" name="email" class="form-control"  placeholder="info@mail.com" >
                            </label>
                            <p class="text-danger" error-msg="email"></p>
                        </div>
                        <div class="form-group">
                            <button type="submit">Восстановить</button>
                        </div>
                    </form>
                </div>
             </div>
        </div>
     </div>
     <div class="modal fade" id="authorizationModal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="authorizationModalTitle">
        <div class="modal-dialog  modal-dialog-centered" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="authorizationModalTitle">Авторизация</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <i class="fal fa-times"></i>
                     </button>
                 </div>
                 <div class="modal-body">
                    <form id="authorizationForm">
                        <div class="form-group">
                            <label>Введите почту
                                <input type="email" name="email" class="form-control"  placeholder="info@mail.com" >
                            </label>
                            <p class="text-danger" error-msg="email"></p>
                        </div>
                        <div class="form-group">
                            <label>Введите пароль
                                <input type="password" name="password" class="form-control" >
                            </label>
                            <p class="text-danger" error-msg="password"></p>
                        </div>
                        <div class="form-group">
                            <button type="submit">Войти</button>
                        </div>
                        <div class="form-group">
                            <a href="#"  data-dismiss="modal" data-toggle="modal" data-target="#registrationModal">Нет аккаунта? Пройдите регистрацию</a>
                        </div>
                        <div class="form-group">
                            <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#passwordResetModal">Забыли пароль?</a>
                        </div>
                    </form>
                </div>
             </div>
        </div>
     </div>
     <div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="registrationModalTitle">
        <div class="modal-dialog  modal-dialog-centered" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="registrationModalTitle">Регистрация</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <i class="fal fa-times"></i>
                     </button>
                 </div>
                 <div class="modal-body">
                    <form id="registrationForm">
                        <div class="form-group">
                            <label>Фамилия
                                <input type="text" name="surname" class="form-control" >
                            </label>
                            <p class="text-danger" error-msg="surname"></p>
                        </div>
                        <div class="form-group">
                            <label>Имя
                                <input type="text" name="name" class="form-control" >
                            </label>
                            <p class="text-danger" error-msg="name"></p>
                        </div>
                        <div class="form-group">
                            <label>E-mail
                                <input type="email" name="email" class="form-control"  placeholder="info@mail.com" >
                            </label>
                            <p class="text-danger" error-msg="email"></p>
                        </div>
                        <div class="form-group">
                            <label>Телфон
                                <input type="tel" name="phone" class="form-control"  placeholder="8 777 777 77 77" >
                            </label>
                            <p class="text-danger" error-msg="phone"></p>
                        </div>
                        <div class="form-group">
                            <label>Пароль
                                <input type="password" name="password" class="form-control" >
                            </label>
                            <p class="text-danger" error-msg="password"></p>
                        </div>
                        <div class="form-group">
                            <label>Подтвердите пароль
                                <input type="password" name="password_confirmation" class="form-control" >
                            </label>
                            <p class="text-danger" error-msg="password_confirmation"></p>
                        </div>
                        <div class="form-group justify-content-center">
                            <button type="submit">Зарегистрироваться</button>
                        </div>
                    </form>
                </div>
             </div>
        </div>
     </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/authentication.js')}}"></script>
    @if (session('order_success'))
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        Swal.fire({
            icon: 'success',
            title: "{{ session('order_success') }}",
            showConfirmButton: false,
            timer: 2000
        });
    </script>
    @endif
    @if (session('order_failure'))
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Ошибка',
            text: "{{ session('order_failure') }}"
        });
    </script>
    @endif
    @if (auth()->check() && auth()->user()->isAdmin())
    <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
    <script src="{{ asset('js/pagebuilder.js') }}"></script>
    @endif
    @stack('scripts')
</body>
</html>
