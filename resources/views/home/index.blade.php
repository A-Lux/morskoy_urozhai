@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/home.css') }}">
@endpush

@section('content')
<div class="home spaced">
    <section class="py-5">
        <div class="container">
            <div class="row">
                @isset($banners[0])
                <div class="col-lg-6 col-md-6 mb-4">
                    <a href="{{ url($banners[0]->url) }}" style="display: contents">
                        <div class="advantage-card" style="background: url({{ asset('storage/' . $banners[0]->background_image) }}), rgb(255, 126, 0); background-position: 100% 25%;">
                            <div class="content-block">
                                <h2>{{ $banners[0]->title }}</h2>
                                <p>{{ $banners[0]->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endisset
                @isset($banners[1])
                <div class="col-lg-6 col-md-6 mb-4">
                    <a href="{{ url($banners[1]->url) }}" style="display: contents">
                        <div class="advantage-card" style="background: url({{ asset('storage/' . $banners[1]->background_image) }}), rgb(75, 174, 80); background-position: 110% 50%;">
                            <div class="content-block">
                                <h2>{{ $banners[1]->title }}</h2>
                                <p>{{ $banners[1]->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endisset
                @isset($banners[2])
                <div class="col-lg-6 col-md-6 mb-4">
                    <a href="{{ url($banners[2]->url) }}" style="display: contents">
                        <div class="advantage-card" style="background: url({{ asset('storage/' . $banners[2]->background_image) }}), rgb(0, 126, 255); background-position: 120% 50%;">
                            <div class="content-block">
                                <h2>{{ $banners[2]->title }}</h2>
                                <p>{{ $banners[2]->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endisset
                @isset($banners[3])
                <div class="col-lg-6 col-md-6 mb-4">
                    <a href="{{ url($banners[3]->url) }}" style="display: contents">
                        <div class="advantage-card" style="background: url({{ asset('storage/' . $banners[3]->background_image) }}), rgb(249, 72, 83); background-position: 450% 0%;">
                            <div class="content-block">
                                <h2>{{ $banners[3]->title }}</h2>
                                <p>{{ $banners[3]->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endisset
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="categories slider">
                        @foreach ($popupCategories as $category)
                        <div>
                            <a href="{{ route('catalog.show', $category->slug) }}">
                                <img data-lazy="{{ asset('storage/' . $category->icon) }}" style="background-color: {{ $category->background_color }}">
                                <p>{{ $category->name }}</p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="background-azure py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h1>Наше спецпредложение</h1>
                    <ul class="offers-nav">
                        <li>
                            <button type="button" class="offer-prev"></button>
                        </li>
                        <li>
                            <button type="button" class="offer-next"></button>
                        </li>
                    </ul>
                </div>
                <div class="col-12 mt-4">
                    <div class="offers slider">
                        @foreach ($offers as $offer)
                        <div class="offer-card">
                            <div class="offer-header">
                                <a href="{{ route('products.show', $offer->slug) }}">
                                    @if ($offer->price < $offer->old_price)
                                    <div class="sale">
                                        <p class="previous-price">{{ $offer->old_price }} тг</p>
                                        <p class="special-price">{{ $offer->price }} тг</p>
                                    </div>
                                    @endif
                                    <img data-lazy="{{ asset('storage/' . $offer->thumbnail) }}">
                                </a>
                            </div>
                            <div class="offer-body p-3">
                                <p>{{ $offer->name }}</p>
                                <span class="vendore-code">Штрих код {{ $offer->sku }}</span>
                                <div class="d-flex align-items-center justify-content-between mt-4">
                                    <p class="price">@if ($offer->discount) {{ $offer->disount }} @else {{ $offer->price }} @endif тг</p>
                                    <button product-price="{{ $offer->price }}" in-stock="{{ $offer->in_stock }}" data-product-id="{{ $offer->id }}" type="button" class="cart-btn"><img src="{{ asset('images/header/cart.png') }}"></button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <h1>Популярные товары</h1>
                </div>
                <div class="col-12 mt-4">
                    @include('partials.popular')
                </div>
            </div>
        </div>
    </section>
    @if ($mainBlog)
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <article>
                        <h2>{{ $mainBlog->title }}</h2>
                        <p>{{ $mainBlog->bodyShortened() }}</p>
                        <a class="read-more" href="{{ route('blog.show', $mainBlog->slug) }}">Читать полностью</a>
                    </article>
                </div>
            </div>
        </div>
    </section>
    @endif
    <section class="background-picture py-5" style="--bg-image: url({{ asset('images/sales.png') }});">
        <div class="container py-5">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <h1><span>Товары со скидкой</span></h1>
                </div>
                <div class="col-12 mt-4">
                    <div class="product slider sales">
                        @foreach ($discounts as $offer)
                        <div class="product-card">
                            <div class="product-header">
                                <a href="{{ route('products.show', $offer->slug) }}">
                                    @if ($offer->price < $offer->old_price)
                                    <span class="tag">{{ $offer->getDiscountPercentage($offer->old_price, $offer->price) }} %</span>
                                    @endif
                                    <img data-lazy="{{ asset('storage/' . $offer->thumbnail) }}">
                                </a>
                            </div>
                            <div class="product-body p-3">
                                <p>{{ $offer->name }}</p>
                                <span class="vendore-code">Штрих код {{ $offer->sku }}</span>
                                <div class="d-flex align-items-center justify-content-between mt-4">
                                    <div>
                                        <p class="price">@if ($offer->price < $offer->old_price) {{ $offer->price }} @else {{ $offer->old_price }} @endif тг</p>
                                        @if ($offer->price < $offer->old_price)
                                        <p class="old-price">{{ $offer->old_price }} тг</p>
                                        @endif
                                    </div>
                                    <button product-price="{{ $offer->price }}" in-stock="{{ $offer->in_stock }}" data-product-id="{{ $offer->id }}" type="button" class="cart-btn"><img src="{{ asset('images/header/cart.png') }}"></button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 pageblock">
                    <article data-type="@pageBlock(['block' => 'main-p1', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'main-p1', 'property' => 'slug'])">
                        @pageBlock(['block' => 'main-p1', 'property' => 'content'])
                    </article>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


@push('scripts')
<script src="{{ mix('js/home.js') }}"></script>
@endpush
