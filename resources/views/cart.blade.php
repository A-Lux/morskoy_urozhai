@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/cart.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="cart">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item active">Корзина</li>
                        </ul>
                    </div>
                    <div class="col-12">
                    <h1>Корзина</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <cart-component></cart-component>
        </section>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ mix('js/cart.js') }}"></script>
@endpush
