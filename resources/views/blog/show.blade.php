@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('css/blog.css') }}">
@endpush
@section('title', $blog->title)
@section('description', $blog->bodyShortened())
@section('content')
<div id="app" class="spaced">
    <div class="blog">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">{{ $blog->title }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="blog-content overflow-auto pb-5">
                    <h1>{{ $blog->title }}</h1>
                    <div class="blog-image">
                        <img src="{{ asset('storage/' . $blog->thumbnail) }}" alt="{{ $blog->title }}">
                    </div>
                    <div class="blog-text">
                    {!! $blog->body !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
