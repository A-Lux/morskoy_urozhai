@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('/css/blog.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="blog">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Блог</li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <h1>Блог</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="blog-content">
                    @foreach ($blogs as $blog)
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="blog-image">
                                <img src="{{ asset('storage/' . $blog->thumbnail) }}" alt="{{ $blog->title }}">
                            </div>
                        </div>
                        <div class="col-xl-8">
                            <div class="blog-text">
                            <h1>{{ $blog->title }}</h1>
                            <p class="blog-info">{{ $blog->bodyShortened() }}</p>
                            <a href="{{ route('blog.show', $blog->slug) }}">Подробнее <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="py-4">
                    {!! $blogs->links() !!}
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
