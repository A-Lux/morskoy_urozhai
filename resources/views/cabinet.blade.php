@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/cabinet.css') }}">
@endpush
@section('title', 'Личный кабинет')
@section('basePath', route('cabinet'))

@section('content')
<div id="app" class="spaced">
    <div class="cabinet">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Личный кабинет</li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <h1>Личный кабинет</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <cabinet-component></cabinet-component>
        </section>
    </div>

</div>
@endsection


@push('scripts')
    <script src="{{ mix('js/cabinet.js') }}"></script>
@endpush
