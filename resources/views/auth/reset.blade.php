@extends('layouts.app')

@section('content')
<div class="py-4 container spaced" id="reset_container">
    <div class="row">
        <div class="offset-lg-3"></div>
        <div class="col-lg-6">
            <h1 class="h1">Восстановление пароля</h1>
        </div>
        <div class="offset-lg-3"></div>
    </div>
    <div class="row">
        <div class="offset-lg-3"></div>
        <div class="col-lg-6">
            <form id="password_reset_form">
                <div class="form-group">
                    <input type="hidden" name="token"  value="{{ request()->token }}" class="form-control">
                    <span class="text-danger error error-token"></span>
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="E-mail" value="{{ request()->email }}" class="form-control">
                    <span class="text-danger error error-email"></span>
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Пароль" class="form-control">
                    <span class="text-danger error error-password"></span>
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" placeholder="Подтвердите пароль" class="form-control">
                </div>
                <button class="btn btn-primary">Восстановить</button>
            </form>
        </div>
        <div class="offset-lg-3"></div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/js/passwordReset.js') }}"></script>
@endpush
