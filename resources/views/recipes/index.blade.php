@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/recipes.css') }}">
@endpush

@section('content')
<div id="app" class="spaced">
    <div class="recipes">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Рецепты</li>
                        </ul>
                    </div>
                    <div class="col-12">
                    <h1>Рецепты</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 order-1 order-sm-0">
                        <nav class="categories" nav-title="Категории">
                            <ul>
                                @foreach ($categories as $categoryItem)
                                @if(isset($category))
                                    @if ($category->id == $categoryItem->id)
                                    <li class="active">
                                        <a href="{{ route('recipe.by-category', $categoryItem->slug) }}">{{ $categoryItem->name }}</a>
                                    </li>
                                    @endif
                                @else
                                <li>
                                    <a href="{{ route('recipe.by-category', $categoryItem->slug) }}">{{ $categoryItem->name }}</a>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </nav>
                        <div class="best-recipes my-5">
                            <h3>Лучшие рецепты недели</h3>
                            <ol>
                                @foreach ($popularRecipes as $recipe)
                                <li>
                                    <a href="{{ route('recipe.show', [
                                            'category' => $recipe->reviewable->category->slug,
                                            'recipe' => $recipe->reviewable->slug
                                        ])}}">
                                        <div class="recipe">
                                            <img src="{{ asset('storage/' . $recipe->reviewable->thumbnail) }}">
                                            <p>{{ $recipe->reviewable->name }}</p>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-6 col-12 mb-4 mb-sm-0">
                        <recipes-component :categories="{{ $categories }}"></recipes-component>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ mix('js/recipes.js') }}"></script>
@endpush
