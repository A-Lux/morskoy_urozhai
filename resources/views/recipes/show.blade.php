@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/recipes.css') }}">
@endpush
@section('title', $recipe->name)
@section('description', $recipe->getShortDescription())
@section('content')
<div id="app" class="spaced">
    <div class="recipes">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">{{ $recipe->name }}</li>
                        </ul>
                    </div>
                    <div class="col-12">
                    <h1>{{ $recipe->name }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-12 order-1 order-lg-0">
                        <nav class="categories" nav-title="Категории">
                            <ul>
                                @foreach ($popularRecipes as $recipe)
                                <li>
                                    <a href="{{ route('recipe.show', [
                                            'category' => $recipe->reviewable->category->slug,
                                            'recipe' => $recipe->reviewable->slug
                                        ])}}">
                                        <div class="recipe">
                                            <img src="{{ asset('storage/' . $recipe->reviewable->thumbnail) }}">
                                            <p>{{ $recipe->reviewable->name }}</p>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </nav>
                        <div class="best-recipes my-5">
                            <h3>Лучшие рецепты недели</h3>
                            <ol>
                                @foreach ($popularRecipes as $recipe)
                                <li>
                                    <a href="{{ route('recipe.show', [
                                            'category' => $recipe->reviewable->category->slug,
                                            'recipe' => $recipe->reviewable->slug
                                        ])}}">
                                        <div class="recipe">
                                            <img src="{{ asset('storage/' . $recipe->reviewable->thumbnail) }}">
                                            <p>{{ $recipe->reviewable->name }}</p>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    <div class="col-lg-9 recipe">
                        <div class="row">
                            <div class="col-lg-7 col-md-6">
                                <img src="{{ asset('storage/' . $recipe->thumbnail) }}">
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <h2 class="title">Ингредиенты</h2>
                                @if ($recipe->ingredients)
                                <ol class="properties my-4">
                                    @foreach ($recipe->ingredients as $ingredient)
                                    <li>
                                        <span class="key">{{ $ingredient['ingredient'] }}</span>
                                        <span></span>
                                        <span class="value">{{ $ingredient['amount'] }}</span>
                                    </li>
                                    @endforeach
                                </ol>
                                @endif
                                <button class="cart-btn" type="button" data-products="@json($recipe->products)">Купить ингредиенты</button>
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="col-lg-5 col-12">
                                <div class="description">
                                    {!! $recipe->description !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @isset($recipe->steps)
                            <div class="col-12">
                                <h2>Способ приготовления</h2>
                                @foreach ($recipe->steps as $step)
                                <div class="step">
                                    <img src="{{ asset('storage/' . $step->thumbnail) }}">
                                    <div>
                                        {!! $step->description !!}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @endisset
                            <div class="col-lg-12 py-5">
                                <reviews-component :product="{{ $recipe->id }}" :type="'recipe'" :average="{{ $recipe->rating }}"></reviews-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ mix('js/reviews.js') }}"></script>
<script src="{{ mix('js/recipe.js') }}"></script>
@endpush
