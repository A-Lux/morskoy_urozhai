@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/card.css') }}">
@endpush
@section('title', $product->name)
@section('description', $product->getShortDescription())
@section('content')
<div id="app" class="spaced">
    <div class="card">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">{{ $product->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="slider slider-for">
                            @php
                                $images = json_decode($product->slider_images, true);
                                $images = $images ?: [];
                            @endphp
                            <div>
                                <img data-lazy="{{ asset('storage/' . $product->thumbnail) }}" alt="{{ $product->name }}">
                            </div>
                            @foreach ($images as $image)
                            <div>
                                <img data-lazy="{{ asset('storage/' . $image) }}" alt="{{ $product->name }}">
                            </div>
                            @endforeach
                        </div>
                        <div class="slider slider-nav">
                            <div>
                                <img data-lazy="{{ asset('storage/' . $product->thumbnail) }}" alt="{{ $product->name }}">
                            </div>
                            @foreach ($images as $image)
                            <div>
                                <img data-lazy="{{ asset('storage/' . $image) }}" alt="{{ $product->name }}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <h2 class="product-title">{{ $product->name }}</h2>
                        <div class="row my-4">
                            <div class="col-lg-3 d-flex align-items-center">
                                <span class="vendore-code">Артикул: {{ $product->sku }}</span>
                            </div>
                            <div class="col-lg-3 d-flex align-items-center">
                                @if ($product->in_stock)
                                <span class="available">В наличии</span>
                                @else
                                <span class="not-available">Нет в наличии</span>
                                @endif
                            </div>
                            <div class="col-lg-3 d-flex align-items-center justify-content-between">
                                <span class="mark">Оценка</span>
                                <div class="star-rating">
                                    @for ($i = 1; $i <= 5; $i++)
                                    @if ($i <= $product->rating)
                                    <label class="star-rating__star is-selected">
                                        <i class="fas fa-star"></i>
                                    </label>
                                    @else
                                    <label class="star-rating__star">
                                        <i class="fas fa-star"></i>
                                    </label>
                                    @endif
                                    @endfor
                                  </div>
                            </div>
                            <div class="col-lg-3 d-flex align-items-center">
                            <button class="favorites-btn" product-id="{{ $product->id }}" type="button">В избранное</button>
                            </div>
                        </div>
                        <div class="border-bottom"></div>
                        <div class="row my-4">
                            <div class="col-lg-9">
                                <ol class="properties">
                                    @foreach ($specifications as $specification)
                                    <li>
                                        <span class="key">{{ $specification->name }}:</span>
                                        <span></span>
                                        @if ($specification->entity)
                                        <span class="value">{{ $specification->entity->name }}</span>
                                        @else
                                        <span class="value">{{ $specification->value }} {{ $specification->dimention }}</span>
                                        @endif
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        <div class="border-bottom"></div>
                        <div class="row my-4">
                            <div class="col-lg-9 d-flex justify-content-between">
                                <div class="prices">
                                    <h5>Цена:</h5>
                                    <ol>
                                        @if ($product->price < $product->old_price)
                                        <li>
                                            <p class="old-price">{{ $product->old_price }} <span>тг</span></p>
                                        </li>
                                        <li>
                                            <p class="price">{{ $product->price }} <span>тг</span></p>
                                        </li>
                                        @else
                                        <li>
                                            <p class="price">{{ $product->price }} <span>тг</span></p>
                                        </li>
                                        @endif
                                    </ol>
                                </div>
                            <button class="cart-btn" type="button" product-price="{{ $product->price }}" in-stock="{{ $product->in_stock }}" product-id="{{ $product->id }}">В корзину</button>
                            </div>
                            <div class="col-lg-3">
                                <div id="product_quantity_controls" data-weighted="{{ $product->is_weighted }}">
                                    <button>-</button>
                                    <input type="number" value="1">
                                    <button>+</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-description-tab" data-toggle="tab" href="#nav-description" role="tab" aria-controls="nav-description" aria-selected="true">Описание продукта</a>
                                <a class="nav-item nav-link" id="nav-reviews-tab" data-toggle="tab" href="#nav-reviews" role="tab" aria-controls="nav-reviews" aria-selected="false">Отзывы (<span>{{ $product->reviews_count }}</span>)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">{!! $product->description !!}</div>
                            <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">
                            <reviews-component :product="{{ $product->id }}" :type="'product'" :average="{{ $product->rating }}"></reviews-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5 background-azure">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <h1>Популярные товары</h1>
                    </div>
                    <div class="col-12 mt-4">
                        @include('partials.popular')
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ mix('js/reviews.js') }}"></script>
<script src="{{ mix('js/card.js') }}"></script>
@endpush
