@extends('layouts.app')


@section('content')
@php
    $blocks = \DB::table('page_blocks')->get()->groupBy('slug')
@endphp
<div class="pageblock">
    <h1 style="text-align: center" data-type="header" data-hash="{{ $blocks['about-header'][0]->slug }}">{{ $blocks['about-header'][0]->content }}</h1>
</div>
<div class="row" style="display: flex; flex-wrap: wrap">
    <div class="col-md-3" style="">
        <div class="pageblock" style="display: flex; justify-content: center">
            <img src="{{ asset('storage/'. $blocks['about-img'][0]->content) }}" data-type="img" data-hash="{{ $blocks['about-img'][0]->slug }}" width="150" height="150">
        </div>
        <div class="pageblock">
            <p data-type="txt" data-hash="{{ $blocks['about-txt'][0]->slug }}">{{ $blocks['about-txt'][0]->content }}</p>
        </div>
    </div>
    <div class="col-md-3" style="">
        <div class="pageblock" style="display: flex; justify-content: center" data-type="img" data-hash="{{ $blocks['about-img'][0]->slug }}">
            <img src="{{ asset('storage/'. $blocks['about-img'][0]->content) }}" data-type="img" data-hash="{{ $blocks['about-img'][0]->slug }}" width="150" height="150">
        </div>
        <div class="pageblock">
            <p data-type="txt" data-hash="{{ $blocks['about-txt'][0]->slug }}">{{ $blocks['about-txt'][0]->content }}</p>
        </div>
    </div>
    <div class="col-md-3" style="">
        <div class="pageblock" style="display: flex; justify-content: center" data-type="img" data-hash="{{ $blocks['about-img'][0]->slug }}">
            <img src="{{ asset('storage/'. $blocks['about-img'][0]->content) }}" data-type="img" data-hash="{{ $blocks['about-img'][0]->slug }}" width="150" height="150">
        </div>
        <div class="pageblock">
            <p data-type="txt" data-hash="{{ $blocks['about-txt'][0]->slug }}">{{ $blocks['about-txt'][0]->content }}</p>
        </div>
    </div>
</div>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>

        <div class="links">
            <a href="https://laravel.com/docs">Docs</a>
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://laravel-news.com">News</a>
            <a href="https://blog.laravel.com">Blog</a>
            <a href="https://nova.laravel.com">Nova</a>
            <a href="https://forge.laravel.com">Forge</a>
            <a href="https://vapor.laravel.com">Vapor</a>
            <a href="https://github.com/laravel/laravel">GitHub</a>
        </div>
    </div>
</div>
@if (auth()->check() && auth()->user()->isAdmin())
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>    
<script src="/js/pagebuilder.js"></script>
@endif
@endsection