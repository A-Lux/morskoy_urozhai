<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>Заказ №{{ $order->id }}</title>
    <style type="text/css" media="all">
        table th, table td {
            padding: 5px;
        }
        table td {
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }
        table tr:last-of-type {
            border-bottom: 0;
        }
    </style>
</head>
<body style="
    margin: 0;
    ">
        <div style="">
            <div style="max-width: 400px; width: 100%; border: 1px solid #000; overflow: hidden;">
                <div style="border-top: 1px solid #000;border-bottom: 1px solid #000; padding: 5px">
                    <div style="
                        display: flex;
                        justify-content: space-between;
                        ">
                        <span style="
                            padding: 0 20px;
                            float: left;
                            width: 50%;
                            ">веб-сайт: m-u.kz</span><span style="float: right; text-align: right;  width: 50%;">WA {{ setting('site.whatsapp') }}</span>
                    </div>
                    <div style="width: 100%; text-align: center; clear: both;">
                        <span>@morskoi.urojai_alatau.et</span>
                    </div>
                </div>
                <div style="
                    width: 100%;
                    ">
                    <div style="text-align: center; margin: 20px 0">Заказ №{{ $order->id }}</div>
                    @if ($order->user)
                    <div style="
                    display: flex;
                    flex-direction: column;
                    justify-content: space-between;
                    margin: 20px 0;
                    "><b style="
                        font-size: 20px;
                        width: 50%;
                        float: left;
                    ">Имя клиента: <span style="float: right; text-align: right; ">{{ $order->user->name }}</span></b>
                    </div>
                    @endif
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;
                        float: left;
                        width: 50%;
                        ">Тел:</b><span style="float: right; text-align: right; width: 50%;">{{ $order->phone }}</span></div>
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    clear: both;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;float: left; width: 50%;
                        ">Тип Доставки:</b><span style="float: right; text-align: right;  width: 50%;">{{ $order->deliveryType->name }}</span></div>
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    clear: both;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;
                        width: 50%;
                        float: left;
                        ">Заказ создан:</b><span style="float: right; text-align: right;  width: 50%;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->format('H:i d-m-Y') }}</span></div>
                    @php
                        if($order->delivery_type == 1) {
                            $info = $order->delivery;
                        } else {
                            $info = $order->pickup;
                        }
                    @endphp

                    @if ($order->delivery_type == 1)
                    <div style="
                    display: flex;
                    clear: both;
                    margin: 20px 0;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;
                        float: left;
                        width: 50%;
                        ">Дата доставки:</b> <span style="width: 50%; float: right; text-align: right; ">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->delivery->deliver_at)->format('H:i d-m-Y') }}</span></div>
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    justify-content: space-between;
                    clear: both;
                    "><b style="
                        font-size: 20px;
                        float: left;
                        width: 50%;
                        flex: 0 0 50%;
                        ">Адрес:</b><label style="flex: 0 0 50%; text-align: right; width: 50%; float: right; text-align: right; ">ул. {{ $order->delivery->street }},
                        {{ $order->delivery->house_building ?  ' корпус ' . $order->delivery->house_building : '' }}
                        {{ $order->delivery->porch ? ', подъезд ' . $order->delivery->porch : '' }}
                        {{ $order->delivery->floor ? ', эт. ' . $order->delivery->floor : '' }}
                        дом {{  $order->delivery->house }}
                        {{ $order->delivery->apartments ? ', кв. ' . $order->delivery->apartments : '' }}
                        {{ $order->delivery->office ? ', офис ' . $order->delivery->office : '' }}</label>
                    </div>
                    @else
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    clear: both;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;
                        float: left;
                        ">Дата доставки:</b> <span style="float: right; text-align: right; ">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->pickup->deliver_at)->format('H:i d-m-Y') }}</span></div>
                    <div style="
                    display: flex;
                    margin: 20px 0;
                    clear: both;
                    justify-content: space-between;
                    "><b style="
                        font-size: 20px;
                        flex: 0 0 50%;
                        float: left;
                        width: 50%;
                        ">Пункт выдачи:</b><label style="text-align: right; float: right; text-align: right; ">
                            {{ $order->pickup->pickpoint->name }},
                            {{ $order->pickup->pickpoint->address }},
                            <div>{{ $order->pickup->pickpoint->phone }},</div>
                            {{ $order->pickup->pickpoint->worktime }},
                            {{ $order->pickup->pickpoint->email }}
                        </label>
                    </div>
                    @endif  
                    <table style="border-collapse: collapse; border-top: 1px solid #000; clear: both;">
                        <tbody>
                            <tr>
                                <th style="border: 1px solid black;">Наименование</th>
                                <th style="border: 1px solid black;">количество</th>
                                <th style="border: 1px solid black;">Цена</th>
                                <th style="border: 1px solid black;">Сумма</th>
                            </tr>
                            @foreach ($order->orderDetails as $product)
                            <tr>
                                <td>{{ $product->product->name }}</td>
                                <td style="text-align: center">{{ $product->unit_quantity }}</td>
                                <td>{{ $product->unit_price }}</td>
                                <td style="text-align: center">{{ $product->unit_quantity * $product->unit_price }}</td>
                            </tr>
                            @endforeach
                            @php
                                $total = 0;
                                foreach($order->orderDetails as $product) {
                                    $total += $product->unit_price * $product->unit_quantity;
                                }
                            @endphp
                            <tr>
                                <td colspan="2">Итого к оплате:</td>
                                <td colspan="2" style="text-align: end">{{ $total }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">{{ $order->paymentType->name }}:</td>
                                <td colspan="2" style="text-align: end">{{ $total }}</td>
                            </tr>
                            <tr>
                                <td colspan="1" style="vertical-align: top">Примечание:</td>
                                <td colspan="3" style="text-align: left">{{ $order->comment }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</body>
</html>