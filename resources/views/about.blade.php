@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('/css/about.css') }}">
@endpush

@section('content')

<div id="app" class="spaced">
    <div class="contacts">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb py-4">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active">@pageBlock(['block' => 'about-header', 'property' => 'content'])</li>
                        </ul>
                    </div>
                    <div class="col-12 pageblock">
                        <h1 data-type="@pageBlock(['block' => 'about-header', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-header', 'property' => 'slug'])">@pageBlock(['block' => 'about-header', 'property' => 'content'])</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="about">
                <div class="container">
                    <div class="about-banner pageblock" style="background-image: url({{ asset('storage') }}/@pageBlock(['block' => 'about-bg-image', 'property' => 'content']))" data-type="@pageBlock(['block' => 'about-bg-image', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-bg-image', 'property' => 'slug'])">
                        <img src="{{ asset('/images/about-logo.png') }}">
                        <div class="about-text pageblock">
                            <div data-type="@pageBlock(['block' => 'about-banner-header', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-banner-header','property' => 'slug'])">
                                @pageBlock(['block' => 'about-banner-header','property' => 'content'])
                            </div>
                        </div>
                    </div>
                    <div class="about-content pageblock">
                        <p data-type="@pageBlock(['block' => 'about-p1', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p1', 'property' => 'slug'])">
                            @pageBlock(['block' => 'about-p1', 'property' => 'content'])
                        </p>
                    </div>
                    <div class="about-advantages">
                        <h1>Почему морепродукты в Алматы купить нужно именно у нас</h1>
                        <div class="about-content pageblock">
                            <p data-type="@pageBlock(['block' => 'about-p2', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p2', 'property' => 'slug'])">
                                @pageBlock(['block' => 'about-p2', 'property' => 'content'])
                            </p>
                        </div>
                        <div class="advantages-list">
                            <div class="advantages-num">
                                <span>1</span>
                            </div>
                            <div class="advantages-text pageblock">
                                <p data-type="@pageBlock(['block' => 'about-p3', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p3', 'property' => 'slug'])">
                                    @pageBlock(['block' => 'about-p3', 'property' => 'content'])
                                </p>
                            </div>
                        </div>
                        <div class="advantages-list">
                            <div class="advantages-num">
                                <span>2</span>
                            </div>
                            <div class="advantages-text pageblock">
                                <p data-type="@pageBlock(['block' => 'about-p4', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p4', 'property' => 'slug'])">
                                    @pageBlock(['block' => 'about-p4', 'property' => 'content'])
                                </p>
                            </div>
                        </div>
                        <div class="advantages-list">
                            <div class="advantages-num">
                                <span>3</span>
                            </div>
                            <div class="advantages-text pageblock">
                                <p data-type="@pageBlock(['block' => 'about-p5', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p5', 'property' => 'slug'])">
                                    @pageBlock(['block' => 'about-p5', 'property' => 'content'])
                                </p>
                            </div>
                        </div>
                        <div class="advantages-list">
                            <div class="advantages-num">
                                <span>4</span>
                            </div>
                            <div class="advantages-text pageblock">
                                <p data-type="@pageBlock(['block' => 'about-p6', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p6', 'property' => 'slug'])">
                                    @pageBlock(['block' => 'about-p6', 'property' => 'content'])
                                </p>
                            </div>
                        </div>
                        <div class="advantages-list">
                            <div class="advantages-num">
                                <span>5</span>
                            </div>
                            <div class="advantages-text pageblock">
                                <p data-type="@pageBlock(['block' => 'about-p7', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p7', 'property' => 'slug'])">
                                    @pageBlock(['block' => 'about-p7', 'property' => 'content'])
                                </p>
                            </div>
                        </div>
                        <div class="about-content pageblock">
                            <p data-type="@pageBlock(['block' => 'about-p8', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p8', 'property' => 'slug'])">
                                @pageBlock(['block' => 'about-p8', 'property' => 'content'])
                            </p>
                        </div>
                        <h1>Оптовый рыбный магазин морепродуктов</h1>
                        <div class="about-content pageblock">
                            <p data-type="@pageBlock(['block' => 'about-p9', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p9', 'property' => 'slug'])">
                                @pageBlock(['block' => 'about-p9', 'property' => 'content'])
                            </p>
                        </div>
                        <div class="about-content pageblock">
                            <div data-type="@pageBlock(['block' => 'about-p10', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p10', 'property' => 'slug'])">
                                @pageBlock(['block' => 'about-p10', 'property' => 'content'])
                            </div>
                        </div>
                        <div class="assortment">
                            <div class="row">
                                <div class="col-xl-3 col-md-6">
                                    <div class="assortment-info">
                                        <img src="{{ asset('/images/done.png') }}" alt="">
                                        <div class="about-content pageblock">
                                            <p data-type="@pageBlock(['block' => 'about-p11', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p11', 'property' => 'slug'])">@pageBlock(['block' => 'about-p11', 'property' => 'content'])</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="assortment-info">
                                        <img src="{{ asset('/images/done.png') }}" alt="">
                                        <div class="about-content pageblock">
                                            <p data-type="@pageBlock(['block' => 'about-p12', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p12', 'property' => 'slug'])">@pageBlock(['block' => 'about-p12', 'property' => 'content'])</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="assortment-info">
                                        <img src="{{ asset('/images/done.png') }}" alt="">
                                        <div class="about-content pageblock">
                                            <p data-type="@pageBlock(['block' => 'about-p13', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p13', 'property' => 'slug'])">@pageBlock(['block' => 'about-p13', 'property' => 'content'])</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="assortment-info">
                                        <img src="{{ asset('/images/done.png') }}" alt="">
                                        <div class="about-content pageblock">
                                            <p data-type="@pageBlock(['block' => 'about-p14', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p14', 'property' => 'slug'])">@pageBlock(['block' => 'about-p14', 'property' => 'content'])</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-info">
                            <div class="about-content pageblock">
                                <span data-type="@pageBlock(['block' => 'about-p15', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p15', 'property' => 'slug'])">@pageBlock(['block' => 'about-p15', 'property' => 'content'])</span>
                            </div>
                            <div class="pageblock">
                                <img src="{{ asset('storage') }}/@pageBlock(['block' => 'about-img1', 'property' => 'content'])"  data-type="@pageBlock(['block' => 'about-img1', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-img1', 'property' => 'slug'])" alt="">
                            </div>
                        </div>
                        <div class="about-info-bottom">
                            <div class="row">
                                <div class="col-xl-5 col-12 col-md-5">
                                    <div class="seafood-image pageblock">
                                        <img src="{{ asset('storage') }}/@pageBlock(['block' => 'about-img2', 'property' => 'content'])" data-type="@pageBlock(['block' => 'about-img2', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-img2', 'property' => 'slug'])" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-7 col-12 col-md-7">
                                    <div class="seafood-text">
                                        <h2>Морепродукты в Алматы: оптом и в розницу</h2>
                                        <div class="pageblock">
                                            <p data-type="@pageBlock(['block' => 'about-p16', 'property' => 'name'])" data-hash="@pageBlock(['block' => 'about-p16', 'property' => 'slug'])">@pageBlock(['block' => 'about-p16', 'property' => 'content'])</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
